# Ticket Inspector

## Prerequisites
Hello,
My name is Mateusz Dobrowolski and in my free time I am working on my own warning system. I am creating the backend using Spring
Framework and Hibernate and frontend using Angular 6 (TypeScript).

At the moment the system has five endpoints (REST) and one websocket endpoint to share information about bus stops, bus lines,
ticket inspection and eg.

The idea is that each person, who has the appliction on their mobile phones, writes the informtion
where the inspection (on the bus) will be. When you see the controler at the bus stop, all you have
to do is to click a name of a bus stop you are at and it finds your localization. So when you get on the
bus with the controllers, other users can trak them and know where they are.

- - -

## Frameworks
* Spring 
* Hibernate
* Angular 6 
* Dozer

- - -

## Author
Mateusz Dobrowolski - mateusz.dobrowolski13@gmail.com
Skype - braders25

