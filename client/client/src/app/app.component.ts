import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {BusStop} from "./bus-stop/models/bus_stop";
import {SystemMessageService} from "./warning/service/system-message.service";
import {NavigationEnd, Router} from "@angular/router";
import {AlertManager} from "./warning/shared/AlertManager";
import {AppInjector} from "../app-injector";
import {LocationService} from "./location/service/location.service";
import {interval} from "rxjs/index";
import {NGXLogger} from "ngx-logger";
import {retryWhen, take} from "rxjs/operators";
import {Meta} from '@angular/platform-browser';

@Component({
  providers: [],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private logger: NGXLogger;
  @ViewChild('warning', {read: ElementRef, static: true}) warning: ElementRef;
  title = 'Ticket Inspector - Canary';
  navbarCollapsed = true;
  busStops: BusStop[];
  alertService: AlertManager;
  isLocationError = false;

  constructor(private router: Router, private systemMessageService: SystemMessageService, logger: NGXLogger, private meta: Meta) {
    this.alertService = new AlertManager(systemMessageService);
    this.logger = logger
    this.meta.addTag({name: "viewport", content: "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"})
  }

  ngOnInit(): void {
    AppInjector.get(LocationService).currentLocationSubscriber.pipe(
      retryWhen(errors => {
        this.isLocationError = true;
        return interval(2000)
      }),
      take(1)
    ).subscribe(value => {this.isLocationError = false});

    this.router.events.forEach(event => {
      if (event instanceof NavigationEnd) {
        this.alertService.clearList()
      }
    })
  }
}
