import {BrowserModule} from '@angular/platform-browser';
import {Injector, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BusStopComponent} from './bus-stop/bus-stop.component';
import {MatTableModule} from '@angular/material/table';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from "@angular/common/http";
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule, MatDialogModule, MatInputModule, MatPaginatorModule, MatSortModule} from '@angular/material';
import {SystemMessageComponent} from './warning/component/system-message.component';
import {AgmCoreModule} from "@agm/core";
import {AgmSnazzyInfoWindowModule} from '@agm/snazzy-info-window';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {AgmJsMarkerClustererModule} from "@agm/js-marker-clusterer";
import {LeafletModule} from "@asymmetrik/ngx-leaflet";
import {LeafletMarkerClusterModule} from '@asymmetrik/ngx-leaflet-markercluster';
import {BusPopupComponent} from './leaflet/popup/busstop/bus-popup.component';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {AppRoutingModule} from "./route/app-routing.module";
import {setAppInjector} from "../app-injector";
import {TicketInspectorPopupComponent} from './leaflet/popup/ticket_inspector/ticket-inspector-popup.component';
import {DistancePipe} from "./pipes/distance-pipe";
import {MatStepperModule} from '@angular/material/stepper';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginModule} from "./login/login.module";
import {CustomElementRegistry} from "../custom-element-registry";

@NgModule({
  declarations: [
    AppComponent,
    BusStopComponent,
    SystemMessageComponent,
    BusPopupComponent,
    TicketInspectorPopupComponent,
    DistancePipe
  ],
  imports: [
    LoginModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    HttpClientModule,
    MatProgressBarModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA7ts39XtS_-kml6-hBSQuPikxo4IVINoY'
    }),
    LeafletModule.forRoot(),
    AgmSnazzyInfoWindowModule,
    AgmJsMarkerClustererModule,
    LeafletMarkerClusterModule.forRoot(),
    MatDialogModule,
    MatButtonModule,
    MatExpansionModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    LoggerModule.forRoot({level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR}),
    MatStepperModule
  ],
  entryComponents: [
    BusPopupComponent,
    TicketInspectorPopupComponent,
  ],
  providers: [CustomElementRegistry],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(injector: Injector) {
    setAppInjector(injector);
  }
}

