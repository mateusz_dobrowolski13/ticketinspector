import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from "rxjs/internal/Observable";
import {AuthorizationState} from "./states/authorization-state";
import {UnauthorizedState} from "./states/unauthorized-state";
import {Router} from "@angular/router";
import {AuthorizedState} from "./states/authorized-state";
import {environment} from "../../environments/environment";

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  private static TOKEN_ENDPOINT = "/oauth/token";
  private _authenticationState: AuthorizationState;

  private httpHeaders : HttpHeaders;

  constructor(private http: HttpClient, private router: Router) {
    let token = localStorage.getItem('token');
    this._authenticationState = token ? new AuthorizedState(this, this.router) : this._authenticationState = new UnauthorizedState(this.router);
    this.httpHeaders = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded")
      .set("Authorization", `Basic ${environment.BASIC64}`);
  }

  get authenticationState(): AuthorizationState {
    return this._authenticationState;
  }

  set authenticationState(value: AuthorizationState) {
    this._authenticationState = value;
  }

  login(username: string, password: string): Observable<any> {
    const body = new HttpParams()
      .set('grant_type', "password")
      .set('username', username)
      .set('password', password);

    return this.http.post(environment.SERVER_URL + AuthenticationService.TOKEN_ENDPOINT,
      body,
      {headers: this.httpHeaders}
    )
      .pipe(map(token => {
        localStorage.setItem('token', JSON.stringify(token));
        this.authenticationState = new AuthorizedState(this, this.router);
        return token;
      }));
  }

  getRefreshTokenRequest(): HttpRequest<any> {
    const body = new HttpParams()
      .set('grant_type', "refresh_token")
      .set('refresh_token', this.getToken().refresh_token);
    return new HttpRequest<any>('POST', 'https://ticket.inspector.com:8443/oauth/token',
      body,
      {headers: this.httpHeaders}
    )
  }

  getToken() {
    return JSON.parse(localStorage.getItem('token'));
  }

  logout() {
    localStorage.removeItem('token');
  }
}
