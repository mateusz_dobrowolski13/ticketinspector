import {Observable} from "rxjs/internal/Observable";
import {HttpHandler, HttpRequest} from "@angular/common/http";

export interface AuthorizationState {

  process(request: HttpRequest<any>, next: HttpHandler) : Observable<any>
}
