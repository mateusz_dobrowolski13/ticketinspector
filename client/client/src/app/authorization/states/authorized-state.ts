import {AuthorizationState} from "./authorization-state";
import {Observable} from "rxjs/internal/Observable";
import {HttpHandler, HttpRequest, HttpResponse} from "@angular/common/http";
import {RefreshTokenInProgressState} from "./refresh-token-in-progress-state";
import {UnauthorizedState} from "./unauthorized-state";
import {catchError, skip, switchMap} from "rxjs/operators";
import {AuthenticationService} from "../authentication-service";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import {Router} from "@angular/router";
import {AppInjector} from "../../../app-injector";
import {SystemMessageService} from "../../warning/service/system-message.service";
import {ErrorAlert} from "../../warning/shared/alerts/error_alert";
import {of} from "rxjs/internal/observable/of";

export class AuthorizedState implements AuthorizationState {

  private _authenticationService: AuthenticationService;
  private _router: Router;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );

  constructor(authenticationService: AuthenticationService, router: Router) {
    this._authenticationService = authenticationService;
    this._router = router;
  }

  process(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    this._authenticationService.authenticationState = new RefreshTokenInProgressState(this.refreshTokenSubject);
    this.refreshTokenSubject.next(null);
    return next.handle(this._authenticationService.getRefreshTokenRequest())
      .pipe(
        skip(1),
        //switch and subscribe the new observable with a new token
        switchMap((token : HttpResponse<any>) => {
          localStorage.setItem('token', JSON.stringify(token.body));
          this._authenticationService.authenticationState = new AuthorizedState(this._authenticationService, this._router);
          this.refreshTokenSubject.next(token.body.access_token);
          return next.handle(request.clone({setHeaders: {Authorization: `Bearer ${token.body.access_token}`}}))
        }),
        catchError(error => {
          this._authenticationService.logout();
          this._router.navigate(["/login"]);
          this._authenticationService.authenticationState = new UnauthorizedState(this._router);
          AppInjector.get(SystemMessageService).sendAlert(new ErrorAlert('Your session has expired. Log in again.'));
          return of([]);
        }));
  }
}
