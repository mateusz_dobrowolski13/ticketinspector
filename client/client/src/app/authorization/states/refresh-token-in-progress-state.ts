import {AuthorizationState} from "./authorization-state";
import {Observable} from "rxjs/internal/Observable";
import {filter, switchMap, take} from "rxjs/operators";
import {HttpHandler, HttpRequest} from "@angular/common/http";

export class RefreshTokenInProgressState implements AuthorizationState {

  private refreshTokenSubject: Observable<string>;

  constructor(refreshTokenSubject: Observable<string>) {
    this.refreshTokenSubject = refreshTokenSubject;
  }

  process(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    return this.refreshTokenSubject.pipe(
      filter(value => value !== null),
      take(1),
      switchMap((token) => {
        console.log(token);
        return next.handle(request.clone({setHeaders: {Authorization: `Bearer ${token}`}}))
      }))
  }

}

