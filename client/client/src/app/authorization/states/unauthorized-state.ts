import {AuthorizationState} from "./authorization-state";
import {Observable} from "rxjs/internal/Observable";
import {throwError} from "rxjs/internal/observable/throwError";
import {Router} from "@angular/router";
import {HttpHandler, HttpRequest} from "@angular/common/http";

export class UnauthorizedState implements AuthorizationState{

  private _router : Router;

  constructor(router : Router) {
    this._router = router;
  }

  process(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    this._router.navigate(["/login"]);
    return throwError("Unauthorized access");
  }
}
