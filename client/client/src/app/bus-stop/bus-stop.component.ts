import {Component, Injector, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {HttpErrorResponse} from "@angular/common/http";
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {SystemMessageService} from "../warning/service/system-message.service";
import {ErrorAlert} from "../warning/shared/alerts/error_alert";
import * as L from 'leaflet';
import {latLng, Layer, Map, Marker as LeafLetMarker, tileLayer} from 'leaflet';
import 'leaflet.markercluster';
import {NGXLogger} from "ngx-logger";
import {LocationService} from "../location/service/location.service";
import {SocketService} from "../webclient/shared/service/socket.service";
import {TicketInspectorMarkerCacheImpl} from "../webclient/shared/model/ticket-inspector-marker-cache-impl";
import {TicketInspectorMarkerCache} from "../webclient/shared/model/ticket-inspector-marker-cache";
import {MarkerType} from "../leaflet/marker/marker-type.enum";
import {BusStopResourceService} from "../rest-service/service/bus-stop-resource-service";
import {Resource} from "../rest-service/model/resource";
import {MarkerFactory} from "../leaflet/marker/marker-factory";
import {CanaryLayer} from "../leaflet/map/layer/canary-layer";
import {delay, retryWhen} from "rxjs/operators";
import {AppInjector} from "../../app-injector";
import {LeafletUtil} from "../leaflet/utils/leaflet-util";
import {TicketNotificationResource} from "../rest-service/model/ticket-notification-resource";
import {interval} from "rxjs/internal/observable/interval";
import {CustomElementRegistry} from "../../custom-element-registry";

@Component({
  selector: 'app-bus-stop',
  templateUrl: './bus-stop.component.html',
  styleUrls: ['./bus-stop.component.css']
})

export class BusStopComponent implements OnInit, OnDestroy {
  private LOGGER_CLASS_PREFIX = "BusStopComponent";
  private ticketInspectorMarkerCache: TicketInspectorMarkerCache;
  private canaryLayer: CanaryLayer;
  private map: Map;
  private busStops: Layer[] = [];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  public downloadData;
  public isTicketInspectorsTableOpened: boolean = false;

  dataSource = new MatTableDataSource();
  displayColumns = ['busStopName', 'busLine', 'distance'];

  markerClusterData: any[] = [];
  markerClusterOptions: L.MarkerClusterGroupOptions;
  theNearestTicketInspection: any;

  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; OpenStreetMap contributors'
      })
    ],
    chunkedLoading: true,
    zoom: 16,
    center: latLng([52.2288, 21.0014])
  };

  constructor(private logger: NGXLogger, private locationService: LocationService, private injector: Injector,
              private systemMessageService: SystemMessageService, private socketService: SocketService, private busStopRestService: BusStopResourceService, private customElementRegistry: CustomElementRegistry) {
  }

  ngOnInit() {
    this.downloadData = true;
    this.busStopRestService.getList().subscribe(data => this.createAndPopulateNewMarkers(data), error => {
        if (error instanceof HttpErrorResponse) {
          this.systemMessageService.sendAlert(new ErrorAlert(error.message));
          this.logger.error(`${this.LOGGER_CLASS_PREFIX}: Error occurred during download bus stops ${error.message}`);
          this.downloadData = false
        }
      }, () => {
        this.downloadData = false;
      }
    );
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

    this.subscribeNotifications()
  }

  rotate(data: any) {
    return LeafletUtil.bearingInDegrees(this.canaryLayer.getLatLng(), data)
  }

  protected subscribeNotifications() {
    this.socketService.getNotificationSubject().subscribe((receiveMessage) => {
      this.logger.info(`${this.LOGGER_CLASS_PREFIX}: Receive the following message ${receiveMessage}`);
      let ticketNotificationResource: TicketNotificationResource = <TicketNotificationResource>JSON.parse(receiveMessage.body);
      this.ticketInspectorMarkerCache.updateLocation(ticketNotificationResource);
      this.refreshMatTable()
    });
  }

  private createAndPopulateNewMarkers(resources: Resource[]) {
    this.busStops = [];
    let markerFactory = MarkerFactory.getMarker(MarkerType.BUS_STOP);
    for (const busStopResource of resources) {
      let newMarker = markerFactory.createLayer(busStopResource);
      let busPopup = this.customElementRegistry.BusPopup;
      newMarker.on("click", busPopup.getContentPopup.bind(busPopup, newMarker, busStopResource));
      this.busStops.push(newMarker)
    }
    this.markerClusterData = this.busStops;
  }

  onMapReady(map: Map) {
    this.map = map;
    this.ticketInspectorMarkerCache = new TicketInspectorMarkerCacheImpl(map);
    this.canaryLayer = new CanaryLayer(map);

    AppInjector.get(LocationService).currentLocationSubscriber.pipe(
      retryWhen(error => {
        return interval(3000)
      }))
      .subscribe((position: Position) => {
        this.canaryLayer.updateLayer(position);
        this.refreshMatTable();
      });

  }

  refreshMatTable(_delay: number = 0) {
    this.ticketInspectorMarkerCache.getValues()
      .pipe(delay(_delay))
      .subscribe(data => {
        let array = Array.from(data);
        array.forEach(data => data.distance = LeafletUtil.getDistance(this.canaryLayer.getLatLng(), (data.marker as LeafLetMarker).getLatLng()));
        this.dataSource.data = array.sort((a, b) => a.distance - b.distance);
        this.theNearestTicketInspection = this.dataSource.data[0]
      });
  }

  ngOnDestroy(): void {
    this.socketService.unsubscribeNotification();
  }


}

