import {Injectable} from "@angular/core";
import {HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {catchError} from "rxjs/operators";
import {AuthenticationService} from "../../authorization/authentication-service";
import {throwError} from "rxjs/internal/observable/throwError";

@Injectable({providedIn: "root"})
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    return next.handle(req).pipe(catchError(error => {
        //if user is unauthorized
        if (error.status === 401) {
          if(req.params.has('refresh_token')) {
            return throwError(error);
          }
          console.log(this.authenticationService.authenticationState);
          return this.authenticationService.authenticationState.process(req, next);
        }
        error.message = 'Service is unavailable';
        return throwError(error);
      })
    )
  }
}
