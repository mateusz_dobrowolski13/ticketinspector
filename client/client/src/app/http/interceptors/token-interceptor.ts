import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/internal/Observable";
import {AuthenticationService} from "../../authorization/authentication-service";

@Injectable({providedIn: "root"})
export class TokenInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let tokenPackage = this.authenticationService.getToken();
    if (!req.headers.get('Authorization') && tokenPackage && tokenPackage.access_token) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${tokenPackage.access_token}`
        }
      });
    }
    return next.handle(req);
  }
}
