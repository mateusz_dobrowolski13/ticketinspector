import {IMarker} from "../../marker/IMarker";
import {LatLng, Map, Marker as LeafLetMarker} from "leaflet";

export class AbstractLayer {
  private markerFactory: IMarker;
  private leaftletMarker: LeafLetMarker;
  private map: Map;


  constructor(markerFactory: IMarker, map: Map) {
    this.markerFactory = markerFactory;
    this.map = map;
  }

  public updateLayer(position: Position) {
    if (this.leaftletMarker != null) {
      this.leaftletMarker.setLatLng([position.coords.latitude, position.coords.longitude]);
    } else {
      this.leaftletMarker = this.markerFactory.createLayer(position);
      this.map.addLayer(this.leaftletMarker);
      this.map.setView(this.leaftletMarker.getLatLng(), 16);
    }
  }

  protected removeLayer() {
    this.map.removeLayer(this.leaftletMarker);
  }

  public getLatLng() : LatLng {
    return this.leaftletMarker.getLatLng();
  }
}
