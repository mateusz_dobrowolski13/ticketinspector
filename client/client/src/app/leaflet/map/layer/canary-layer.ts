import {Map} from "leaflet";
import {AbstractLayer} from "./abstract-layer";
import {MarkerFactory} from "../../marker/marker-factory";
import {MarkerType} from "../../marker/marker-type.enum";

export class CanaryLayer extends AbstractLayer {

  constructor(map: Map) {
    super(MarkerFactory.getMarker(MarkerType.CANARY), map);
  }

  protected removeLayer(): void {
    super.removeLayer();
  }
}
