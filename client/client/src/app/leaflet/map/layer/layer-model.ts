import {Marker as LeafLetMarker, Polyline} from "leaflet";

export interface LayerModel {
  createDate: Date;
  updateDate: Date;
  marker: LeafLetMarker;
  route: Polyline;
  distance?: number
  busStopResource: { busStopName: string, busStopLine: string }
}
