import {Marker as LeafLetMarker} from "leaflet";

export interface IMarker {
  createLayer(data: any): LeafLetMarker;
}

