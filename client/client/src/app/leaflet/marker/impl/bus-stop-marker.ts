import {IMarker} from "../IMarker";
import {icon, Marker as LeafLetMarker, marker} from "leaflet";

export class BusStopMarker implements IMarker {

  createLayer(data: any): LeafLetMarker {
    return marker([data.latitude, data.longitude], {
      icon: icon({
        iconUrl: 'assets/icons/bus-stop.png',
        iconAnchor: [16, -5]
      }),
      title: data.name
    });
  }
}
