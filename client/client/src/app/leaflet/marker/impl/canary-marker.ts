import {IMarker} from "../IMarker";
import {icon, Marker as LeafLetMarker, marker} from "leaflet";

export class CanaryMarker implements IMarker {


  createLayer(data: Position): LeafLetMarker {
    const newMarker = marker([data.coords.latitude, data.coords.longitude], {
      icon: icon({
        iconUrl: 'assets/icons/bird.png',
        iconSize: [30, 30]
      }),
      title: "You"
    });
    return newMarker;
  }
}
