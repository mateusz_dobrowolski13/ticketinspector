import {IMarker} from "../IMarker";
import {icon, Marker as LeafLetMarker, marker} from "leaflet";
import {LocationMessage} from "../../../webclient/shared/model/location.message";

export class TicketInspectorMarker implements IMarker {

  createLayer(data: LocationMessage): LeafLetMarker {
    const newMarker = marker([data.latitude, data.longitude], {
      icon: icon({
        iconUrl: 'assets/icons/sylvester.png',
        iconSize: [50, 50]
      }),
      title: data.busLine
    });
    return newMarker;
  }
}
