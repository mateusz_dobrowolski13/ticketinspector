import {MarkerType} from "./marker-type.enum";
import {BusStopMarker} from "./impl/bus-stop-marker";
import {TicketInspectorMarker} from "./impl/ticket-inspector-marker";
import {IMarker} from "./IMarker";
import {CanaryMarker} from "./impl/canary-marker";

export class MarkerFactory {

  public static getMarker(markerType: MarkerType): IMarker {
    switch (markerType) {
      case MarkerType.BUS_STOP :
        return new BusStopMarker();
      case MarkerType.TICKET_INSPECTOR :
        return new TicketInspectorMarker();
      case MarkerType.CANARY :
        return new CanaryMarker();
    }
  }
}
