import {Component, Input, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BusStop} from "../../../bus-stop/models/bus_stop";
import {MatDialog, MatStepper} from "@angular/material";
import {NGXLogger} from "ngx-logger";
import {BusStopLineResourceService} from "../../../rest-service/service/bus-stop-line-resource-service";
import {BusStopLinesResource} from "../../../rest-service/model/bus-stop-lines-resource";
import {HttpErrorResponse} from "@angular/common/http";
import {LatLng} from "leaflet";
import {LocationService} from "../../../location/service/location.service";
import {AbstractButtonState} from "./button/states/abstract-button-state";
import {SpyButtonState} from "./button/states/spy-button-state";
import {StopButtonState} from "./button/states/stop-button-state";
import {TrackingButtonHandler} from "./button/handlers/tracking-button-handler";

@Component({
  selector: 'app-leafletpopup',
  templateUrl: './bus-popup.component.html',
  styleUrls: ['./bus-popup.component.css']
})

export class BusPopupComponent implements OnInit, OnDestroy {
  private LOGGER_CLASS_PREFIX = "BusPopupComponent";
  private _buttonState: AbstractButtonState;
  private _currentBusLine: string = "";
  private _busStopCoordinates : LatLng;
  _message : string;

  @Input()
  busStop: BusStop;
  @ViewChild(MatStepper, {static: true})
  stepper : MatStepper;

  constructor(private logger: NGXLogger, private busStopLineResource: BusStopLineResourceService, private dialog: MatDialog, private zone: NgZone, private locationService : LocationService) {
  }

  get title(): String {
    let title = this.busStop.name;
    if (this.currentBusLine.length !== 0) {
      title += " - " + this.currentBusLine
    }
    return title
  }

  set buttonState(value: AbstractButtonState) {
    this._buttonState = value;
  }

  goToTrackingStep(line: string) {
    if (this.locationService.lastUserLocation.distanceTo(this._busStopCoordinates) < 300) {
      this._currentBusLine = line;
      this._buttonState = new SpyButtonState(new TrackingButtonHandler(this.busStop, line));
      this.stepper.next();
    } else {
      this._message = "You are too far away from the bus stop"
    }
  }

  get currentBusLine(): string {
    return this._currentBusLine;
  }

  get buttonClass(): string {
    return this._buttonState != null ? this._buttonState._buttonClass : "";
  }

  get buttonText(): string {
    return this._buttonState != null ? this._buttonState._buttonText : "";
  }

  public busStopLinesResource: BusStopLinesResource = new BusStopLinesResource();
  public downloadData: boolean;
  public isError = false;
  private errorMessage: string;

  ngOnInit() {
    this._busStopCoordinates = new LatLng(this.busStop.latitude, this.busStop.longitude);
    this.zone.run(() => {
      this.downloadData = true;
      this.busStopLineResource.getResource(this.busStop.id).subscribe(response =>
          this.busStopLinesResource = response, error => {
          if (error instanceof HttpErrorResponse) {
            this.isError = true;
            this.downloadData = false;
            this.errorMessage = error.error;
            this.logger.error(`${this.LOGGER_CLASS_PREFIX}: Error has been occurred with the following error message: ${this.errorMessage}`)
          }
        },
        () => {
          this.downloadData = false;
        }
      )
    })
  }

  clickTrackButton(): void {
    this.zone.run(() => {
      this._buttonState.invoke(this);
    })
  }

  goStepperBack() {
    this.zone.run(() => {
      this.stopSpying();
      this._currentBusLine = "";
      this.stepper.previous()
    });
  }

  ngOnDestroy(): void {
    this.stopSpying();
  }

  private stopSpying() {
    if (this._buttonState instanceof StopButtonState) {
      this._buttonState.invoke(this);
    }
  }
}
