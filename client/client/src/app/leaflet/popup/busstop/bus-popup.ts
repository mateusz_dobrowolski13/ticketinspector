import {Popup} from "../popup";
import {Type} from "@angular/core";
import {BusPopupComponent} from "./bus-popup.component";
import {BusStop} from "../../../bus-stop/models/bus_stop";
import {NgElement, WithProperties} from "@angular/elements";

export class BusPopup extends Popup<BusStop> {

  constructor() {
    super("app-leafletpopup", "bus-popup");
  }

  public get Component(): Type<any> {
    return BusPopupComponent;
  }

  public setProperties(property: BusStop, popupElement: HTMLElement): void {
    (popupElement as NgElement & WithProperties<{ busStop: BusStop }>).busStop = property
  }

}
