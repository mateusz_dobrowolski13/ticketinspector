import {UUID} from "angular2-uuid";
import {LocationMessageBuilder} from "../../../../../webclient/shared/builder/LocationMessageBuilder";
import {BusStop} from "../../../../../bus-stop/models/bus_stop";
import {AppInjector} from "../../../../../../app-injector";
import {LocationService} from "../../../../../location/service/location.service";
import {LocationState} from "../../../../../location/location-state.enum";
import {Subscription} from "rxjs";
import {SocketService} from "../../../../../webclient/shared/service/socket.service";

export class TrackingButtonHandler {
  private locationMessageBuilder: LocationMessageBuilder;
  private locationSubscriber: Subscription;
  private locationService = AppInjector.get(LocationService);
  private socketService = AppInjector.get(SocketService);

  constructor(public busStop: BusStop, public currentLine: string) {
    this.locationMessageBuilder = new LocationMessageBuilder(UUID.UUID(), new Date())
      .withBusLine(this.currentLine)
      .withBusStopName(this.busStop.name);

  }

  public localizeAndSend() {
    console.log(this.locationSubscriber);
    if (this.locationSubscriber == null) {
      this.locationSubscriber = this.locationService.currentLocationSubscriber.subscribe((position: Position) => {
        let cords = position.coords;
        this.socketService.send(this.locationMessageBuilder
          .withLatitude(cords.latitude)
          .withLongitude(cords.longitude)
          .withUpdateDate(new Date())
          .withState(LocationState.UPDATE)
          .build())
      }, error => console.log(`Error : ${error}`));
      this.socketService.unsubscribeNotification();
    }
  }

  public stopLocalizeAndSend() {
    this.socketService.send(this.locationMessageBuilder
      .withLatitude(0)
      .withLongitude(0)
      .withUpdateDate(new Date())
      .withState(LocationState.REMOVE)
      .build());
    this.socketService.subscribeNotification();
    this.locationSubscriber.unsubscribe();
    this.locationSubscriber = null;
  }

}
