import {BusPopupComponent} from "../../bus-popup.component";
import {TrackingButtonHandler} from "../handlers/tracking-button-handler";

export abstract class AbstractButtonState {
  readonly _buttonText: string;
  readonly _buttonClass: string;
  readonly _tackingButtonHandler: TrackingButtonHandler;


  protected constructor(buttonText: string, buttonClass: string, tackingButtonHandler: TrackingButtonHandler) {
    this._buttonText = buttonText;
    this._buttonClass = buttonClass;
    this._tackingButtonHandler = tackingButtonHandler;
  }

  public abstract invoke(busPopupComponent: BusPopupComponent);

}
