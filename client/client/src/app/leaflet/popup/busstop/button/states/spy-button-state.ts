import {AbstractButtonState} from "./abstract-button-state";
import {StopButtonState} from "./stop-button-state";
import {BusPopupComponent} from "../../bus-popup.component";
import {TrackingButtonHandler} from "../handlers/tracking-button-handler";

export class SpyButtonState extends AbstractButtonState {

  constructor(tackingButtonHandler: TrackingButtonHandler) {
    super("Spy", "button-track", tackingButtonHandler)
  }

  invoke(busPopupComponent: BusPopupComponent) {
    this._tackingButtonHandler.localizeAndSend();
    busPopupComponent.buttonState = new StopButtonState(this._tackingButtonHandler);
  }
}
