import {AbstractButtonState} from "./abstract-button-state";
import {SpyButtonState} from "./spy-button-state";
import {BusPopupComponent} from "../../bus-popup.component";
import {TrackingButtonHandler} from "../handlers/tracking-button-handler";

export class StopButtonState extends AbstractButtonState {

  constructor(tackingButtonHandler: TrackingButtonHandler) {
    super("Stop", "button-track-stop", tackingButtonHandler);
  }

  invoke(busPopupComponent: BusPopupComponent) {
    this._tackingButtonHandler.stopLocalizeAndSend();
    busPopupComponent.buttonState = new SpyButtonState(this._tackingButtonHandler);
  }

}
