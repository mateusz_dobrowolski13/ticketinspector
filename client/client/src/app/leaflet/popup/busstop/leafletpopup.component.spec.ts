import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BusPopupComponent} from './bus-popup.component';

describe('BusPopupComponent', () => {
  let component: BusPopupComponent;
  let fixture: ComponentFixture<BusPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
