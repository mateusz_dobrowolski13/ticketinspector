import {createCustomElement, NgElement} from "@angular/elements";
import {AppInjector} from "../../../app-injector";
import {Type} from "@angular/core";
import {Marker} from "leaflet";

export abstract class Popup<T> {
  private readonly customElementName: string;
  private readonly className: string;

  protected constructor(customElementName: string, className: string) {
    this.customElementName = customElementName;
    this.className = className;
    this.createPopupElement()
  }

  private createPopupElement() {
    const popupComponent = createCustomElement(this.Component, {injector: AppInjector});
    customElements.define(this.customElementName, popupComponent);
  }

  public getContentPopup(marker: Marker, property: T, e) {
    let popupElement = document.createElement(this.customElementName) as NgElement;
    this.setProperties(property, popupElement);
    if (marker.getPopup() == undefined) {
      marker.bindPopup(popupElement, {className: this.className}).openPopup()
    } else {
      marker.bindPopup(popupElement, {className: this.className})
    }
  }

  public abstract get Component(): Type<any>;

  public abstract setProperties(property: T, popupElement: HTMLElement): void;

}
