import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TicketInspectorPopupComponent} from './ticket-inspector-popup.component';

describe('TicketInspectorPopupComponent', () => {
  let component: TicketInspectorPopupComponent;
  let fixture: ComponentFixture<TicketInspectorPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketInspectorPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketInspectorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
