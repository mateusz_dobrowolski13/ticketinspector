import {Component, Input, OnInit} from '@angular/core';
import {LayerModel} from "../../map/layer/layer-model";

@Component({
  selector: 'app-canary-popup',
  templateUrl: './ticket-inspector-popup.component.html',
  styleUrls: ['./ticket-inspector-popup.component.css']
})
export class TicketInspectorPopupComponent implements OnInit {

  @Input()
  layerModel: LayerModel;

  constructor() {

  }

  ngOnInit(): void {
  }

}
