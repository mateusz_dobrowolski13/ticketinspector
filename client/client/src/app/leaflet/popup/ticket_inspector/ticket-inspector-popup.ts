import {NgElement, WithProperties} from "@angular/elements";
import {Popup} from "../popup";
import {Type} from "@angular/core";
import {TicketInspectorPopupComponent} from "./ticket-inspector-popup.component";
import {LayerModel} from "../../map/layer/layer-model";

export class TicketInspectorPopup extends Popup<LayerModel> {

  constructor() {
    super("app-canarypopup", "canary-popup");
  }

  public get Component(): Type<any> {
    return TicketInspectorPopupComponent;
  }

  public setProperties(property: LayerModel, popupElement: HTMLElement): void {
    (popupElement as NgElement & WithProperties<{ layerModel: LayerModel }>).layerModel = property
  }

}
