import {LatLng} from "leaflet";

export class LeafletUtil {
  static getDistance(src: LatLng, dst: LatLng) {
    return src.distanceTo(dst)
  }

  public static bearingInRadians(src: LatLng, dst: LatLng) {
    let srcLat = this.toRadians(src.lat);
    let dstLat = this.toRadians(dst.lat);
    let dLng = this.toRadians(dst.lng - src.lng);

    return Math.atan2(Math.sin(dLng) * Math.cos(dstLat),
      Math.cos(srcLat) * Math.sin(dstLat) -
      Math.sin(srcLat) * Math.cos(dstLat) * Math.cos(dLng));
  }

  public static bearingInDegrees(src: LatLng, dst: LatLng) {

    return (this.toDegrees((this.bearingInRadians(src, dst))) + 360) % 360;
  }

  private static toDegrees(radians) {
    return radians * 180 / Math.PI;
  };

  private static toRadians(degree) {
    return degree * Math.PI / 180;
  }
}
