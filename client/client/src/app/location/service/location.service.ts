import {Injectable} from '@angular/core';
import {NGXLogger} from "ngx-logger";
import {Observable} from "rxjs";
import {LatLng} from "leaflet";

@Injectable({
  providedIn: 'root'
})

export class LocationService {

  constructor(private logger: NGXLogger) {
  }

  private _lastUserLocation: LatLng;

  get lastUserLocation(): LatLng {
    return this._lastUserLocation;
  }

  readonly currentLocationSubscriber = new Observable((observer) => {
    let watchId;
    if ('geolocation' in navigator) {
      let navigator: any = window.navigator;

      navigator.permissions.query({name: 'geolocation'}).then((result) => {
        if (result.state !== 'granted') {
          observer.error('Geolocation permission has not granted yet');
        }
        watchId = navigator.geolocation.watchPosition((position => {
          this._lastUserLocation = new LatLng(position.coords.latitude, position.coords.longitude);
          observer.next(position)
        }), (error: PositionError) => observer.error(error.message), {enableHighAccuracy: true});
      });
    } else {
      observer.error('Geolocation not available');
    }
    return {
      unsubscribe() {
        navigator.geolocation.clearWatch(watchId)
      }
    }
  });

}
