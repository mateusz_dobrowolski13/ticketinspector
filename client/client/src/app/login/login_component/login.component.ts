import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Validator} from "./validators/validator";
import {RequiredValidator} from "./validators/required-validator";
import {UsernameValidator} from "./validators/username-validator";
import {HttpErrorResponse} from "@angular/common/http";
import {AuthenticationService} from "../../authorization/authentication-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  _formGroup: FormGroup;
  loginErrorMessage: string;
  private _userNameControl: FormControl;
  private _passwordControl: FormControl;

  constructor(private _formBuilder: FormBuilder, private authenticationService: AuthenticationService, private router: Router) {
  }

  ngOnInit() {
    this._userNameControl = new FormControl('', [Validators.required, Validators.pattern("[^\\s]+")]);
    this._passwordControl = new FormControl('', Validators.required);
    this._formGroup = this._formBuilder.group({
      userNameControl: this._userNameControl,
      passwordControl: this._passwordControl
    });
  }

  checkUsername(): string {
    return this.checkField(Array.of(new RequiredValidator('username'), new UsernameValidator()), this._userNameControl)
  }

  checkPassword(): string {
    return this.checkField(Array.of(new RequiredValidator('password')), this._passwordControl);
  }

  private checkField(validators: Array<Validator>, formControl: FormControl): string {
    for (let validator of validators) {
      if (validator.valid(formControl)) {
        return validator.message();
      }
    }
    return null;
  }

  login() {
    this.authenticationService.login(this._userNameControl.value, this._passwordControl.value)
      .subscribe(value => this.router.navigate(['/']), httpErrorResponse => {
          if (httpErrorResponse instanceof HttpErrorResponse && httpErrorResponse.status == 400) {
            if (httpErrorResponse.error.error == 'invalid_grant')
              this.loginErrorMessage = "You have entered an invalid username or password"
          }
        }
      );
  }

}
