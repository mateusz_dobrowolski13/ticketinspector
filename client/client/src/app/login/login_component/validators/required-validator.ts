import {Validator} from "./validator";
import {FormControl} from "@angular/forms";

export class RequiredValidator implements Validator {
  readonly _fieldName: string;

  constructor(fieldName: string) {
    this._fieldName = fieldName;
  }

  valid(formControl: FormControl) {
    return formControl.hasError('required')
  }

  message(): string {
    return 'You must enter a ' + this._fieldName
  }
}
