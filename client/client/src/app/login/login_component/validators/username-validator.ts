import {FormControl} from "@angular/forms";
import {Validator} from "./validator";

export class UsernameValidator implements Validator {
  message(): string {
    return "Insert a correct username";
  }

  valid(formControl: FormControl): boolean {
    return formControl.hasError('pattern');
  }

}
