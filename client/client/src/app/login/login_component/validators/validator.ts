import {FormControl} from "@angular/forms";

export interface Validator {
  valid(formControl : FormControl) : boolean;
  message() : string;
}
