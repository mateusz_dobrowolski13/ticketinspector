import {Pipe, PipeTransform} from "@angular/core";
import {DecimalPipe} from "@angular/common";

@Pipe({name: "distancePipe"})
export class DistancePipe implements PipeTransform {
  private decimalPipe = new DecimalPipe('en-US')
  transform(value: number, ...args: any[]): string {

    if(value > 1000) {
      return this.formatNumber(value / 1000) + " kilometers"
    }
    return this.formatNumber(value) + " meters"
  }

  private  formatNumber(value : number) : string {
    return this.decimalPipe.transform(value,'1.2-2')
  }
}
