import {Resource} from "./resource";

export class BusStopLine extends Resource {
  busStopID : number;
  busLine : string;
  inspectionTicket : boolean;
}
