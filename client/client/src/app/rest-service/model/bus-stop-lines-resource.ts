import {Resource} from "./resource";
import {BusStopLine} from "./bus-stop-line";

export class BusStopLinesResource extends Resource {
  anyInspectionTicket: boolean;
  busStopLines: BusStopLine[] = [];
}
