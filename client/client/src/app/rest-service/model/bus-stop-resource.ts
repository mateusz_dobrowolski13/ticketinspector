import {Resource} from "./resource";

export class BusStopResource extends Resource {
  name : string;
  latitude : number;
  longitude : number;
  city: string;
}
