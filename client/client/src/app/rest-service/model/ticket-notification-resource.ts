import {Resource} from "./resource";
import {TrackResource} from "./track-resource";

export class TicketNotificationResource extends Resource {
  uuid: string;
  busStopName: string;
  busLine: string;
  latitude: number;
  longitude: number;
  route?: TrackResource[];
  state: string;
  createDate: Date;
  updateDate: Date;
}
