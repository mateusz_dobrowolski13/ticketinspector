import {RestServiceModule} from './rest-service.module';

describe('RestServiceModule', () => {
  let restServiceModule: RestServiceModule;

  beforeEach(() => {
    restServiceModule = new RestServiceModule();
  });

  it('should create an instance', () => {
    expect(restServiceModule).toBeTruthy();
  });
});
