import {Resource} from "../model/resource";
import {BusStopLinesResource} from "../model/bus-stop-lines-resource";

export class BusStopLinesSerializer implements BusStopLinesSerializer {
  fromJson(object: any): Resource {
    let busStopLinesResource = new BusStopLinesResource();
    Object.assign(busStopLinesResource, object);
    return busStopLinesResource;
  }

  toJson(resource: Resource): any {
    return JSON.stringify(resource);
  }

}
