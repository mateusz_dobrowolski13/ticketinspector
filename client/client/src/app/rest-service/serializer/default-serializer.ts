import {Serializer} from "./serializer";
import {Resource} from "../model/resource";

export class DefaultSerializer implements Serializer {
  fromJson(json: any): Resource {
    return JSON.parse(json);
  }

  toJson(resource: Resource): any {
    return JSON.stringify(resource);
  }
}
