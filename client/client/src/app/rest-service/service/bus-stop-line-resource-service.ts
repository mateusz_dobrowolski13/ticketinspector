import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {SubResourceService} from "./sub-resource-service";
import {BusStopLinesResource} from "../model/bus-stop-lines-resource";
import {BusStopLinesSerializer} from "../serializer/bus-stop-lines-serializer";

@Injectable({
  providedIn: 'root'
})

export class BusStopLineResourceService extends SubResourceService<BusStopLinesResource> {

  constructor(httpClient: HttpClient) {
    super(httpClient, "busStop", "lines", new BusStopLinesSerializer())
  }
}

