import {HttpClient} from "@angular/common/http";
import {ResourceService} from "./resource-service";
import {BusStopResource} from "../model/bus-stop-resource";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})

export class BusStopResourceService extends ResourceService<BusStopResource> {

  constructor(httpClient: HttpClient) {
    super(httpClient, "busStops")
  }
}
