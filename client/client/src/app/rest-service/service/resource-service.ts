import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {DefaultSerializer} from "../serializer/default-serializer";
import {Serializer} from "../serializer/serializer";
import {Observable} from "rxjs/internal/Observable";
import {Resource} from "../model/resource";
import {environment} from "../../../environments/environment";

export class ResourceService<T extends Resource> {


  constructor(public httpClient: HttpClient, public endpoint: string, public serializer: Serializer = new DefaultSerializer()) {
  }

  getResource(): Observable<T> {
    return this.httpClient.get(`${environment.SERVER_URL}/rest/${this.endpoint}`).pipe(map(data => this.serializer.fromJson(data) as T));
  }

  getList(): Observable<T[]> {
    return this.httpClient.get(`${environment.SERVER_URL}/rest/${this.endpoint}`).pipe(map((data: any) => data.map(item => item as T) as T[]));
  }
}
