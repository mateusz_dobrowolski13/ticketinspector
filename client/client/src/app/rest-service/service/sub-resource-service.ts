import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {map} from "rxjs/operators";
import {DefaultSerializer} from "../serializer/default-serializer";
import {Serializer} from "../serializer/serializer";
import {Resource} from "../model/resource";
import {environment} from "../../../environments/environment";

export class SubResourceService<T extends Resource> {


  constructor(public httpClient: HttpClient, public parentEndpoint: string, public endpoint: string, public serializer: Serializer = new DefaultSerializer()) {
  }

  getResource(id: number): Observable<T> {
    return this.httpClient.get(`${environment.SERVER_URL}/rest/${this.parentEndpoint}/${id}/${this.endpoint}`).pipe(map(data => this.serializer.fromJson(data) as T));
  }

  getList(): Observable<T[]> {
    return this.httpClient.get(`${environment.SERVER_URL}/rest/${this.endpoint}`).pipe(map((data: any) => data.map(item => item as T) as T[]));
  }
}
