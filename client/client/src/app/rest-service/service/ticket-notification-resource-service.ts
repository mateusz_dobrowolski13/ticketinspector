import {HttpClient} from "@angular/common/http";
import {ResourceService} from "./resource-service";
import {TicketNotificationResource} from "../model/ticket-notification-resource";

export class TicketNotificationResourceService extends ResourceService<TicketNotificationResource> {

  constructor(httpClient: HttpClient) {
    super(httpClient, "ticketNotification")
  }
}
