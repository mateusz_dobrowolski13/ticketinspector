import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "../login/login_component/login.component";
import {AuthGuard} from "../guards/auth-guard";
import {BusStopComponent} from "../bus-stop/bus-stop.component";

const routes: Routes = [
  //{path: 'busStops', component: BusStopComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: '', pathMatch: 'full', component: BusStopComponent, canActivate: [AuthGuard]},
  {path: '**', redirectTo: ''}
];


@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})

export class AppRoutingModule {
}
