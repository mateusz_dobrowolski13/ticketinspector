import {Component, Input, OnInit} from '@angular/core';
import {AlertManager} from "../shared/AlertManager";

@Component({
  selector: 'app-system-message',
  templateUrl: './system-message.component.html',
  styleUrls: ['./system-message.component.css']
})
export class SystemMessageComponent implements OnInit {

  @Input() alertService : AlertManager;

  constructor() { }

  ngOnInit() {
  }


  getAlerts() {
    return this.alertService.getAlerts();
  }

}
