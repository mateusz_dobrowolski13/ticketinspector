import {Subject} from "rxjs/internal/Subject";
import {Injectable} from "@angular/core";
import {Alert} from "../shared/alerts/alert";

@Injectable({
  providedIn: 'root'
})
export class SystemMessageService {
  private source = new Subject<Alert>();

  constructor() { }

  getSource() : Subject<Alert> {
    return this.source;
  }

  sendAlert(alert : Alert) {
    setTimeout(()=> this.source.next(alert),0);
  }
}
