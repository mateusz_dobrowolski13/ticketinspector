import {Alert} from "./alerts/alert";
import {SystemMessageService} from "../service/system-message.service";

export class AlertManager {
  alerts: Array<Alert> = [];

  constructor(private systemMessageService: SystemMessageService) {
    this.systemMessageService.getSource().subscribe(alert => {
      this.alerts.push(alert)
    })
  }

  removeAlert(alert : Alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1)
  }

  clearList(){
    this.alerts = [];
  }

  getAlerts(){
    return this.alerts;
  }
}
