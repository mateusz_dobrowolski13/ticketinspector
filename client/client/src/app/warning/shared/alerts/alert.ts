export enum AlertType {
  SUCCESS = "Success",
  WARNING = "Warning",
  DANGER = "DANGER",
}
export abstract class Alert {
  private readonly _type : AlertType;
  private _message : string;

  protected constructor(type : AlertType, message : string) {
    this._type = type;
    this._message = message;
  }

  get type(): AlertType {
    return this._type;
  }

  protected setMessage(value: string) {
    this._message = value;
  }

  get message(): string {
    return this._message;
  }
}
