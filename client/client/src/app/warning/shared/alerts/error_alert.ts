import {Alert, AlertType} from "./alert";

export class ErrorAlert extends Alert {
  constructor(message : string) {
    super(AlertType.WARNING, message);
  }
}
