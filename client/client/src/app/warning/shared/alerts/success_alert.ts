import {Alert, AlertType} from "./alert";

export class SuccessAlert extends Alert {
  constructor(message : string) {
    super(AlertType.SUCCESS, message);
  }
}
