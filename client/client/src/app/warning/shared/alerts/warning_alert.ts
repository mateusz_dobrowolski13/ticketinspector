import {Alert, AlertType} from "./alert";

export class WarningAlert extends Alert {

  constructor(message : string) {
    super(AlertType.WARNING, message);
  }
}
