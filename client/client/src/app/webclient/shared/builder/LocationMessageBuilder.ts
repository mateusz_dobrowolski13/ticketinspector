import {LocationMessage} from "../model/location.message";
import {TrackResource} from "../../../rest-service/model/track-resource";
import {LocationState} from "../../../location/location-state.enum";

export class LocationMessageBuilder {
  private _locationMessage = new LocationMessage();


  constructor(uuid: string, createDate: Date) {
    this._locationMessage.uuid = uuid;
    this._locationMessage.createDate = createDate;
  }

  withBusLine(busLine: string): LocationMessageBuilder {
    this._locationMessage.busLine = busLine;
    return this
  }

  withBusStopName(busStopName: string): LocationMessageBuilder {
    this._locationMessage.busStopName = busStopName;
    return this
  }

  withLatitude(latitude: number): LocationMessageBuilder {
    this._locationMessage.latitude = latitude;
    return this
  }

  withLongitude(longitude: number): LocationMessageBuilder {
    this._locationMessage.longitude = longitude;
    return this
  }

  withCreateDate(createDate: Date): LocationMessageBuilder {
    this._locationMessage.createDate = createDate;
    return this;
  }

  withUpdateDate(updateDate: Date): LocationMessageBuilder {
    this._locationMessage.updateDate = updateDate;
    return this;
  }

  withRoute(route: TrackResource[]): LocationMessageBuilder {
    this._locationMessage.route = route;
    return this;
  }

  withState(state: LocationState): LocationMessageBuilder {
    this._locationMessage.state = LocationState[state];
    return this;
  }

  build(): LocationMessage {
    return this._locationMessage
  }
}
