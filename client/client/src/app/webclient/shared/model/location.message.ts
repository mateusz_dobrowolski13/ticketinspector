import {TrackResource} from "../../../rest-service/model/track-resource";

export class LocationMessage {
  uuid : string;
  busLine : string;
  busStopName : string;
  latitude ?: number;
  longitude ?: number;
  createDate : Date;
  updateDate : Date;
  state : string;
  route? : TrackResource[];
}
