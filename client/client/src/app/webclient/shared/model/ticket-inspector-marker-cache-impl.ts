import {TicketInspectorMarkerCache} from "./ticket-inspector-marker-cache";
import {LatLng, Map as LeafLetMap, Marker as LeafLetMarker, Polyline} from "leaflet";
import {MarkerType} from "../../../leaflet/marker/marker-type.enum";
import {IMarker} from "../../../leaflet/marker/IMarker";
import {LocationState} from "../../../location/location-state.enum";
import {LayerModel} from "../../../leaflet/map/layer/layer-model";
import {TicketNotificationResourceService} from "../../../rest-service/service/ticket-notification-resource-service";
import {AppInjector} from "../../../../app-injector";
import {HttpClient} from "@angular/common/http";
import {TrackResource} from "../../../rest-service/model/track-resource";
import {MarkerFactory} from "../../../leaflet/marker/marker-factory";
import {TicketNotificationResource} from "../../../rest-service/model/ticket-notification-resource";
import {Observable} from 'rxjs';
import {of} from "rxjs/internal/observable/of";
import {TicketInspectorPopup} from "../../../leaflet/popup/ticket_inspector/ticket-inspector-popup";
import {CustomElementRegistry} from "../../../../custom-element-registry";

export class TicketInspectorMarkerCacheImpl implements TicketInspectorMarkerCache {
  private ticketNotificationResourceService: TicketNotificationResourceService;
  private ticketInspectorMarkerMap: Map<string, LayerModel>;
  private marker: IMarker = MarkerFactory.getMarker(MarkerType.TICKET_INSPECTOR);
  private _map: LeafLetMap;
  private colors: Set<string>;
  private ticketInspectorPopup : TicketInspectorPopup = AppInjector.get(CustomElementRegistry).TicketInspectorPopup;

  constructor(map: LeafLetMap) {
    this._map = map;
    this.initTicketInspectorMarkerMap();
    this.colors = new Set<string>();
  }

  private initTicketInspectorMarkerMap() {
    this.ticketInspectorMarkerMap = new Map<string, LayerModel>();
    this.ticketNotificationResourceService = new TicketNotificationResourceService(AppInjector.get(HttpClient));
    this.ticketNotificationResourceService.getList().subscribe(data => data.forEach(resource => this.updateLayer(resource)))
  }

  updateLocation(ticketNotificationResource: TicketNotificationResource) {
    switch (LocationState[ticketNotificationResource.state]) {
      case LocationState.UPDATE : {
        this.updateLayer(ticketNotificationResource);
        break;
      }
      case LocationState.REMOVE : {
        this.removeLayer(ticketNotificationResource);
        break;
      }
    }
  }

  private updateLayer(ticketNotificationResource: TicketNotificationResource) {
    if (this.ticketInspectorMarkerMap.has(ticketNotificationResource.uuid)) {
      let layerModel = this.ticketInspectorMarkerMap.get(ticketNotificationResource.uuid);
      let marker = layerModel.marker as LeafLetMarker;
      layerModel.updateDate = ticketNotificationResource.updateDate;
      marker.setLatLng([ticketNotificationResource.latitude, ticketNotificationResource.longitude]);
      layerModel.route.addLatLng([ticketNotificationResource.latitude, ticketNotificationResource.longitude]);
    } else {
      let layerModel = this.createMarker(ticketNotificationResource);
      layerModel.marker.on("click", this.ticketInspectorPopup.getContentPopup.bind(this.ticketInspectorPopup, layerModel.marker, layerModel));
      this._map.addLayer(layerModel.marker);
      this._map.addLayer(layerModel.route);
    }
  }

  private createMarker(ticketNotificationResource: TicketNotificationResource): LayerModel {
    let layer = this.marker.createLayer(ticketNotificationResource);
    let color = this.generateColor();
    let polyline = ticketNotificationResource.route ? ticketNotificationResource.route.map((track: TrackResource) => new LatLng(track.latitude, track.longitude)) : [];
    let layerModel: LayerModel = {
      marker: layer,
      createDate: ticketNotificationResource.createDate,
      updateDate: ticketNotificationResource.updateDate,
      busStopResource: {
        busStopName: ticketNotificationResource.busStopName,
        busStopLine: ticketNotificationResource.busLine
      },
      route: new Polyline(polyline, {color: color})
    };
    this.ticketInspectorMarkerMap.set(ticketNotificationResource.uuid, layerModel);
    return layerModel;
  }

  private generateColor(): string {
    let color: string;
    do {
      let r = Math.floor(Math.random() * 255);
      let g = Math.floor(Math.random() * 255);
      let b = Math.floor(Math.random() * 255);
      color = "rgb(" + r + " ," + g + "," + b + ")";
    } while (this.colors.has(color));
    this.colors.add(color);
    return color;
  }

  private removeLayer(ticketNotificationResource: TicketNotificationResource) {
    let uuid = ticketNotificationResource.uuid;
    if (this.ticketInspectorMarkerMap.has(uuid)) {
      let layerModel = this.ticketInspectorMarkerMap.get(uuid);
      this._map.removeLayer(layerModel.route);
      this._map.removeLayer(layerModel.marker);
      this.colors.delete(layerModel.route.options.color);
      this.ticketInspectorMarkerMap.delete(uuid);
    }
  }

  clearAll() {
    this.ticketInspectorMarkerMap.clear()
  }

  getValues(): Observable<IterableIterator<LayerModel>> {
    return of(this.ticketInspectorMarkerMap.values());
  }

}
