import {LayerModel} from "../../../leaflet/map/layer/layer-model";
import {Observable} from "rxjs/internal/Observable";
import {TicketNotificationResource} from "../../../rest-service/model/ticket-notification-resource";

export interface TicketInspectorMarkerCache {
  updateLocation(ticketNotificationResource : TicketNotificationResource);
  getValues() : Observable<IterableIterator<LayerModel>>
  clearAll() : void;
}
