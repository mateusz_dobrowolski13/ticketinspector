import {Injectable} from '@angular/core';
import * as Stomp from '@stomp/stompjs';
import {Client, Frame, StompSubscription} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {LocationMessage} from "../model/location.message";
import {NGXLogger} from "ngx-logger";
import {BusStop} from "../../../bus-stop/models/bus_stop";
import {Observable, Subject} from 'rxjs';
import {environment} from "../../../../environments/environment";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import {filter, last} from "rxjs/operators";

export enum SocketClientState {
  ATTEMPTING, CONNECTED, DISCONNECTED
}

@Injectable({
  providedIn: 'root'
})

export class SocketService {
  private readonly LOGGER_CLASS_PREFIX = "SocketService";
  private readonly WEB_SOCKET_ENDPOINT = "/socket";

  private _stompClient: Client;

  private _uuid: string;
  private _busStop: BusStop;
  private _line: string;
  private _socketServiceStateSubject = new BehaviorSubject<SocketClientState>(SocketClientState.ATTEMPTING);
  private _notificationStompSubscription: StompSubscription;
  private _notificationSubscriptionSubject = new Subject();

  constructor(private logger: NGXLogger) {
    let ws = new SockJS(environment.SERVER_URL + this.WEB_SOCKET_ENDPOINT);
    this._stompClient = Stomp.over(ws);
    this._stompClient.connect({}, this.onConnect, this.onError, this.onDisconnect);
  }

  get uuid(): string {
    return this._uuid;
  }

  get busStop(): BusStop {
    return this._busStop;
  }

  get line(): string {
    return this._line;
  }

  protected onConnect = (frame: Frame) => {
    this.logger.info(`${this.LOGGER_CLASS_PREFIX}: Socket connection succeed`);
    this._stompClient.reconnect_delay = 10000;
    this._socketServiceStateSubject.next(SocketClientState.CONNECTED);
  };

  protected onError = (error: string | Stomp.Message) => {
    this.logger.error(error);
    if (typeof error === 'object') {
      error = (<Stomp.Message>error).body;
    }
    this.logger.error(`${this.LOGGER_CLASS_PREFIX}  ${error}`);
  };

  protected onDisconnect = () => {
    this.logger.info(`${this.LOGGER_CLASS_PREFIX}: Socket is disconnected`);
    this._socketServiceStateSubject.next(SocketClientState.DISCONNECTED);
  };

  public getNotificationSubject(): Observable<any> {
    this.subscribeNotification();
    return this._notificationSubscriptionSubject
  }

  public subscribeNotification() {
    if (!this._notificationStompSubscription) {
      this.getActiveSocketClient().subscribe(client => {
        this._notificationStompSubscription = client.subscribe("/topic/notification", (receivedMessage) => {
          this._notificationSubscriptionSubject.next(receivedMessage);
        });
      })
    }
  }

  public unsubscribeNotification() {
    if (this._notificationStompSubscription) {
      this._notificationStompSubscription.unsubscribe();
      this._notificationStompSubscription = null
    }
  }

  private getActiveSocketClient(): Observable<Client> {
    return new Observable<Client>(observer => {
      this._socketServiceStateSubject.pipe(filter(state => state === SocketClientState.CONNECTED, last())).subscribe(() => observer.next(this._stompClient));
    })
  }

  public send(locationMessage: LocationMessage): void {
    this._stompClient.send("/app/notification", {}, (JSON.stringify(locationMessage)));
  }
}
