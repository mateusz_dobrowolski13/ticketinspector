import {Injectable} from "@angular/core";
import {BusPopup} from "./app/leaflet/popup/busstop/bus-popup";
import {TicketInspectorPopup} from "./app/leaflet/popup/ticket_inspector/ticket-inspector-popup";

@Injectable()
export class CustomElementRegistry {
  private busPopup = new BusPopup();
  private ticketInspectorPopup = new TicketInspectorPopup();

  get BusPopup() {
    return this.busPopup;
  }

  get TicketInspectorPopup() {
    return this.ticketInspectorPopup
  }

}
