package com.braders.persistence.config.database;

import com.braders.persistence.config.properties.DatabaseConfigProperties;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;

import static org.springframework.orm.jpa.vendor.Database.MYSQL;

@Configuration
@EnableAspectJAutoProxy
@EnableTransactionManagement
@RequiredArgsConstructor
@Slf4j
public class ConfigurationJpa {

    private final DatabaseConfigProperties databaseConfigProperties;

    @Bean
    public DataSource dataSource() {
        var pooledDataSource = new ComboPooledDataSource();
        try {
            pooledDataSource.setDriverClass(databaseConfigProperties.driver());
            pooledDataSource.setUser(databaseConfigProperties.userName());
            pooledDataSource.setPassword(databaseConfigProperties.password());
            pooledDataSource.setMinPoolSize(2);
            pooledDataSource.setMaxPoolSize(5);
            pooledDataSource.setJdbcUrl(databaseConfigProperties.url());
            pooledDataSource.setTestConnectionOnCheckout(true);
        } catch (PropertyVetoException e) {
            log.error(e.getMessage(), e);
        }
        return pooledDataSource;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(true);
        hibernateJpaVendorAdapter.setGenerateDdl(false);
        hibernateJpaVendorAdapter.setDatabase(MYSQL);
        return hibernateJpaVendorAdapter;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        localContainerEntityManagerFactoryBean.setDataSource(dataSource());
        localContainerEntityManagerFactoryBean.setPackagesToScan(databaseConfigProperties.entityPackage());
        localContainerEntityManagerFactoryBean.setPersistenceUnitName(databaseConfigProperties.unitName());
        localContainerEntityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter());
        localContainerEntityManagerFactoryBean.setJpaProperties(jpaProperties());
        return localContainerEntityManagerFactoryBean;
    }

    @Bean
    public Properties jpaProperties() {
        var jpaProperties = new Properties();
        jpaProperties.setProperty("hibernate.enable_lazy_load_no_trans","true");
        jpaProperties.setProperty("javax.persistence.sharedCache.mode", "ENABLE_SELECTIVE");
        return jpaProperties;
    }


    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
