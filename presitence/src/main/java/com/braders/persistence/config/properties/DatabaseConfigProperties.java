package com.braders.persistence.config.properties;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
@Getter
@Accessors(fluent = true)
public abstract class DatabaseConfigProperties {

    @Value("${database.url}")
    private String url;

    @Value("${database.username}")
    private String userName;

    @Value("${database.password}")
    private String password;

    @Value("${database.driver}")
    private String driver;

    @Value("${database.entitypackage}")
    private String entityPackage;

    @Value("${database.unitname}")
    private String unitName;

}
