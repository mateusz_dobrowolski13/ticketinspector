package com.braders.persistence.config.properties;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@Profile("dev")
@PropertySource(value = "classpath:default-db-config.properties", ignoreResourceNotFound = true)
public class DefaultDbConfigProperties extends DatabaseConfigProperties {
}
