package com.braders.persistence.config.properties;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@Profile("test")
@PropertySource(value = "classpath:test-db-config.properties", ignoreResourceNotFound = true)
public class TestDbConfigProperties extends DatabaseConfigProperties {
}
