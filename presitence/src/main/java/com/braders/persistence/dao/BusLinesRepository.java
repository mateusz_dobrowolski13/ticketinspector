package com.braders.persistence.dao;

import com.braders.persistence.entity.BusLineEntity;

public interface BusLinesRepository extends GenericRepository<Integer, BusLineEntity> {
}
