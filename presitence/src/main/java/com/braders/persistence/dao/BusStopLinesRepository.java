package com.braders.persistence.dao;

import com.braders.persistence.entity.BusStopLinesEntity;

import java.util.Collection;

public interface BusStopLinesRepository extends GenericRepository<Integer, BusStopLinesEntity> {
    Collection<BusStopLinesEntity> getAllBusStopLines(int id);
    int update(int id, boolean inspectionTicket);
}
