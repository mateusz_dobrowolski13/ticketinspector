package com.braders.persistence.dao;

import com.braders.persistence.entity.BusStopEntity;

import java.util.Collection;

public interface BusStopRepository extends GenericRepository<Integer, BusStopEntity> {
    Collection<BusStopEntity> getNearestBusStops(double longitude, double latitude);
    String getCity(int id);
}
