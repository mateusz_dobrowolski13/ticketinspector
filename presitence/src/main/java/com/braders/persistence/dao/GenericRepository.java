package com.braders.persistence.dao;


import org.springframework.data.repository.Repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;

public interface GenericRepository<T, E extends Serializable> extends Repository<T, E> {

    Optional<E> get(T id);

    Collection<E> getAll();

    boolean insert(E entity);

    boolean delete(T id);

    boolean deleteAll();

}
