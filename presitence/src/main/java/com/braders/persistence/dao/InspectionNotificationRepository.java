package com.braders.persistence.dao;

import com.braders.persistence.entity.InspectionNotificationEntity;

public interface InspectionNotificationRepository extends GenericRepository<Integer, InspectionNotificationEntity> {
}
