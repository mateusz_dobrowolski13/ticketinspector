package com.braders.persistence.dao;

import com.braders.persistence.entity.inspection.TicketInspectionEntity;

public interface TicketInspectionRepository extends GenericRepository<String, TicketInspectionEntity> {
    TicketInspectionEntity update(TicketInspectionEntity ticketInspectionEntity);
}

