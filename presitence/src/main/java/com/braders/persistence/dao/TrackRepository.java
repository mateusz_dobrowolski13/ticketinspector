package com.braders.persistence.dao;

import com.braders.persistence.entity.inspection.TrackEntity;

public interface TrackRepository extends GenericRepository<String, TrackEntity> {
}
