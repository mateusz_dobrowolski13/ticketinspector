package com.braders.persistence.dao.implement.jpa;

import com.braders.persistence.dao.BusLinesRepository;
import com.braders.persistence.entity.BusLineEntity;
import com.braders.persistence.util.database.JpaDBUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

import static com.braders.persistence.util.Const.UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION;

@Repository
@Profile("jpa")
@RequiredArgsConstructor
public class BusLinesRepositoryImpl implements BusLinesRepository {

    private final JpaDBUtil dbUtil;

    @Override
    public Optional<BusLineEntity> get(Integer id) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public Collection<BusLineEntity> getAll() {
        return dbUtil.query(entityManager -> entityManager
                .createQuery("SELECT c FROM BusLineEntity c", BusLineEntity.class)
                .getResultList());
    }

    @Override
    public boolean insert(BusLineEntity busStopEntity) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public boolean delete(Integer integer) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public boolean deleteAll() {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

}
