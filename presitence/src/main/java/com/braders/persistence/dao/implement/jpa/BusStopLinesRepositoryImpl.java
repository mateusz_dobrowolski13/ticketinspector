package com.braders.persistence.dao.implement.jpa;

import com.braders.persistence.dao.BusStopLinesRepository;
import com.braders.persistence.entity.BusStopLinesEntity;
import com.braders.persistence.util.database.JpaDBUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.CacheStoreMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.Optional;

import static com.braders.persistence.util.Const.UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION;

@Repository
@RequiredArgsConstructor
@Profile("jpa")
public class BusStopLinesRepositoryImpl implements BusStopLinesRepository {

    private final JpaDBUtil dbUtil;

    @Override
    public Optional<BusStopLinesEntity> get(Integer id) {
        return Optional.of(dbUtil.query(entityManager -> entityManager.createQuery("from BusStopLinesEntity c WHERE c.id = :id", BusStopLinesEntity.class))
                .setParameter("id", id)
                .setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH)
                .getSingleResult());
    }

    @Override
    public Collection<BusStopLinesEntity> getAll() {
        return dbUtil.query(entityManager -> entityManager
                .createQuery("SELECT c FROM BusStopLinesEntity c", BusStopLinesEntity.class)
                .getResultList());
    }

    @Override
    public boolean insert(BusStopLinesEntity busStopEntity) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public boolean delete(Integer integer) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public int update(int id, boolean inspectionTicket) {
        return dbUtil.query(entityManager -> entityManager.createQuery("UPDATE BusStopLinesEntity SET inspectionTicket = :inspectionTicket WHERE id = :id"))
                .setParameter("id", id)
                .setParameter("inspectionTicket", inspectionTicket)
                .executeUpdate();
    }

    @Override
    public boolean deleteAll() {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public Collection<BusStopLinesEntity> getAllBusStopLines(int busStopID) {
        return dbUtil.query(entityManager -> {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<BusStopLinesEntity> criteriaQuery = criteriaBuilder.createQuery(BusStopLinesEntity.class);
            Root<BusStopLinesEntity> root = criteriaQuery.from(BusStopLinesEntity.class);

            ParameterExpression<Integer> busStopParameterExpression = criteriaBuilder.parameter(Integer.class);
            criteriaQuery.select(root).where((criteriaBuilder.equal(busStopParameterExpression, root.get("fkBusStop"))));
            return entityManager.createQuery(criteriaQuery).setParameter(busStopParameterExpression, busStopID).getResultList();
        });
    }
}
