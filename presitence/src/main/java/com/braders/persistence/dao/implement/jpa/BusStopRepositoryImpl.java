package com.braders.persistence.dao.implement.jpa;

import com.braders.persistence.dao.BusStopRepository;
import com.braders.persistence.entity.BusStopEntity;
import com.braders.persistence.util.database.JpaDBUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.util.Collection;
import java.util.Optional;

import static com.braders.persistence.util.Const.UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION;

@Repository
@Profile("jpa")
@RequiredArgsConstructor
public class BusStopRepositoryImpl implements BusStopRepository {

    private static final String LONGITUDE = "longitude";
    private static final String LATITUDE = "latitude";

    private final JpaDBUtil dbUtil;

    @Override
    public Optional<BusStopEntity> get(Integer id) {
        return dbUtil.query(entityManager -> Optional.ofNullable(entityManager.find(BusStopEntity.class, id)));
    }

    @Override
    public Collection<BusStopEntity> getAll() {
        return dbUtil.query(entityManager -> entityManager
                .createQuery("SELECT c FROM BusStopEntity c", BusStopEntity.class)
                .getResultList());
    }

    @Override
    public boolean insert(BusStopEntity busStopEntity) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public boolean delete(Integer integer) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public boolean deleteAll() {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public Collection<BusStopEntity> getNearestBusStops(double longitude, double latitude) {
        return dbUtil.query(entityManager -> {
            StoredProcedureQuery query = entityManager.createStoredProcedureQuery("nearestBusStops", BusStopEntity.class)
                    .registerStoredProcedureParameter(LONGITUDE, Double.class, ParameterMode.IN)
                    .registerStoredProcedureParameter(LATITUDE, Double.class, ParameterMode.IN);
            query.setParameter(LONGITUDE, longitude);
            query.setParameter(LATITUDE, latitude);
            query.execute();
            return (Collection<BusStopEntity>) query.getResultList();
        });
    }

    @Override
    public String getCity(int id) {
        return dbUtil.query(entityManager -> entityManager
                .createQuery("select c from BusStopEntity c where c.id = :id", BusStopEntity.class)
                .setParameter("id", id)
                .getSingleResult()).getCity();
    }
}
