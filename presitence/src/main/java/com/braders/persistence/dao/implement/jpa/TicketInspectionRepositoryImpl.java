package com.braders.persistence.dao.implement.jpa;

import com.braders.persistence.dao.TicketInspectionRepository;
import com.braders.persistence.entity.inspection.TicketInspectionEntity;
import com.braders.persistence.util.database.JpaDBUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import java.util.Collection;
import java.util.Optional;

import static com.braders.persistence.util.Const.UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION;

@Repository
@RequiredArgsConstructor
@Transactional
@Profile("jpa")
@Slf4j
public class TicketInspectionRepositoryImpl implements TicketInspectionRepository {
    private final JpaDBUtil dbUtil;

    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public Optional<TicketInspectionEntity> get(String uuid) {
        return dbUtil.query(entityManager -> Optional.ofNullable(entityManager.find(TicketInspectionEntity.class, uuid)));
    }

    @Override
    public Collection<TicketInspectionEntity> getAll() {
        return dbUtil.query(entityManager -> entityManager
                .createQuery("SELECT c FROM TicketInspectionEntity c where c.state = true", TicketInspectionEntity.class)
                .getResultList());
    }

    @Override
    public boolean insert(TicketInspectionEntity entity) {
        return dbUtil.query(entityManager -> {
            try {
                entityManager.persist(entity);
            } catch (EntityExistsException exception) {
                log.error("Error occurred during inserting entity", exception);
                return false;
            }
            return true;
        });
    }

    @Override
    public boolean delete(String uuid) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public boolean deleteAll() {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public TicketInspectionEntity update(TicketInspectionEntity ticketInspectionEntity) {
        return dbUtil.query(entityManager -> {
            entityManager.merge(ticketInspectionEntity);
            return ticketInspectionEntity;
        });
    }
}
