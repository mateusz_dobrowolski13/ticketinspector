package com.braders.persistence.dao.implement.jpa;

import com.braders.persistence.dao.TrackRepository;
import com.braders.persistence.entity.inspection.TrackEntity;
import com.braders.persistence.util.database.JpaDBUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
@Transactional
@Profile("jpa")
@Slf4j
public class TrackRepositoryImpl implements TrackRepository {
    private final JpaDBUtil dbUtil;

    @Override
    public Optional<TrackEntity> get(String id) {
        return Optional.empty();
    }

    @Override
    public Collection<TrackEntity> getAll() {
        return Collections.emptyList();
    }

    @Override
    public boolean insert(TrackEntity entity) {
        return dbUtil.query(entityManager -> {
            try {
                entityManager.persist(entity);
            } catch (EntityExistsException exception) {
                log.error("Error occurred during inserting entity", exception);
                return false;
            }
            return true;
        });
    }

    @Override
    public boolean delete(String id) {
        return false;
    }

    @Override
    public boolean deleteAll() {
        return false;
    }
}
