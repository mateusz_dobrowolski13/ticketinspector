package com.braders.persistence.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "bus_line")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BusLineEntity implements Serializable {

    @Id
    @Column(name = "bus_line_id", nullable = false)
    private String busLine;
}
