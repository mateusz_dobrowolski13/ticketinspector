package com.braders.persistence.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "bus_stop")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
@NamedStoredProcedureQuery(
        name = "nearestBusStops",
        procedureName = "nearestBusStops",
        resultClasses = {BusStopEntity.class},
        parameters = {
                @StoredProcedureParameter(name = "longitude", type = Double.class, mode = ParameterMode.IN),
                @StoredProcedureParameter(name = "latitude", type = Double.class, mode = ParameterMode.IN),
        }
)

public class BusStopEntity implements Serializable {
    @Id
    @Column(name = "bus_stop_id", nullable = false)
    private int id;
    private String name;
    private Double latitude;
    private Double longitude;
    private String city;
}
