package com.braders.persistence.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "bus_stops_lines")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BusStopLinesEntity implements Serializable {

    @Id
    @Column(name = "bus_stops_lines_id", nullable = false)
    private int id;

    @Column(name = "fk_bus_stations", nullable = false)
    private int fkBusStop;

    @Column(name = "fk_bus_line", nullable = false, length = 4)
    private String fkBusLine;

    @Column(name = "checking_ticket", nullable = true)
    private Boolean inspectionTicket = false;

    @ManyToOne
    @JoinColumn(name = "fk_bus_line", referencedColumnName = "bus_line_id", nullable = false, insertable = false, updatable = false)
    private BusLineEntity busLine;

    @ManyToOne
    @JoinColumn(name = "fk_bus_stations", referencedColumnName = "bus_stop_id", nullable = false, insertable = false, updatable = false)
    private BusStopEntity busStop;

}
