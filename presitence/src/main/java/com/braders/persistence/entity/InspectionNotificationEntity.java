package com.braders.persistence.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "ticket_inspection_notification")
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class InspectionNotificationEntity implements Serializable {

    @Id
    @Column(name = "ticket_inspection_notification_id", nullable = false)
    private int id;

    @Column(name = "date", nullable = false)
    private LocalDateTime  date;

    @ManyToOne
    @JoinColumn(name = "fk_bus_stop_lines", referencedColumnName = "bus_stops_lines_id", nullable = false)
    private BusStopLinesEntity busStop;

}
