package com.braders.persistence.entity.inspection;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "ticket_inspection")
@AllArgsConstructor
public class TicketInspectionEntity implements Serializable {
    @Id
    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "update_date")
    private Date updateDate;

    @Column(name = "bus_stop_name")
    private String busStopName;

    @Column(name = "bus_line", nullable = false)
    private String busLine;

    @Column(name = "state")
    private boolean state;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_notification_id", referencedColumnName = "uuid")
    private List<TrackEntity> route;

    public TicketInspectionEntity() {
        Date currentDate = new Date();
        this.createDate = currentDate;
        this.updateDate = currentDate;
    }
}
