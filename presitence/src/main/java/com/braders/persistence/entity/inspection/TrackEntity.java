package com.braders.persistence.entity.inspection;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "track")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrackEntity implements Serializable {
    @Column
    @Id
    private int id;

    @Column(name = "fk_notification_id", nullable = false)
    private String fkNotificationId;

    @Column(name = "longitude", nullable = false)
    private double longitude;

    @Column(name = "latitude", nullable = false)
    private double latitude;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_notification_id", referencedColumnName = "uuid", insertable = false, updatable = false)
    private TicketInspectionEntity inspectionEntity;
}
