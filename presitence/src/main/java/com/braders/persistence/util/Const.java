package com.braders.persistence.util;

public class Const {
    public static final String UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION = "No support operation yet";

    private Const() {
    }
}
