package com.braders.persistence.util.database;

import java.util.function.Function;

public interface DBUtil<V> {

    <T> T query(Function<V, T> query);
}
