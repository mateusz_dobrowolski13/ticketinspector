package com.braders.persistence.util.database;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
@RequiredArgsConstructor
@Profile("h2")
public class H2DBUtil implements DBUtil<JdbcTemplate> {

    private final JdbcTemplate jdbcTemplate;

    public <T> T query(Function<JdbcTemplate, T> query) {
        return query.apply(jdbcTemplate);
    }
}
