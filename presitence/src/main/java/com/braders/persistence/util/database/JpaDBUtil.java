package com.braders.persistence.util.database;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.function.Function;

@Component
@Profile("jpa")
public class JpaDBUtil implements DBUtil<EntityManager> {

    @PersistenceContext
    private EntityManager entityManager;

    public <T> T query(Function<EntityManager, T> query) {
        return query.apply(entityManager);
    }

}
