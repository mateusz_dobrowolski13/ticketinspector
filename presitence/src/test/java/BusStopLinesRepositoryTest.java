import com.braders.persistence.config.IntegrationTestConfig;
import com.braders.persistence.dao.BusStopLinesRepository;
import com.braders.persistence.entity.BusStopLinesEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@ActiveProfiles({"test","jpa"})
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {IntegrationTestConfig.class})
@Slf4j
public class BusStopLinesRepositoryTest {

    @Autowired
    private BusStopLinesRepository busStopLinesRepository;

    @Before
    public void setup() {
        busStopLinesRepository = Mockito.mock(BusStopLinesRepository.class, AdditionalAnswers.delegatesTo(busStopLinesRepository));
    }

    @Test
    @Transactional
    @Rollback
    public void testDatabaseOperation() {
        Assert.assertEquals(3748, busStopLinesRepository.getAll().size());
        Mockito.verify(busStopLinesRepository, Mockito.times(1)).getAll();

        Optional<BusStopLinesEntity> busStopLinesEntity = busStopLinesRepository.get(533);

        Assert.assertEquals(true, busStopLinesEntity.isPresent());

        boolean inspectionTicketStatusBeforeUpdate = busStopLinesEntity.get().getInspectionTicket();

        Assert.assertEquals(1, busStopLinesRepository.update(533, !inspectionTicketStatusBeforeUpdate));

        Mockito.verify(busStopLinesRepository, Mockito.times(1)).update(533, !inspectionTicketStatusBeforeUpdate);
        Assert.assertNotEquals(inspectionTicketStatusBeforeUpdate, busStopLinesRepository.get(533).get());

        Assert.assertEquals(3748, busStopLinesRepository.getAll().size());

    }

    @Test(expected = UnsupportedOperationException.class)
    public void deleteAllShouldBeThrownUnsupportedOperationException() {
        busStopLinesRepository.deleteAll();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void deleteShouldBeThrownUnsupportedOperationException() {
        busStopLinesRepository.delete(1023);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void insertShouldBeThrownUnsupportedOperationException() {
        BusStopLinesEntity busStopLinesEntity = BusStopLinesEntity.builder().inspectionTicket(true)
                .fkBusStop(102)
                .build();
        busStopLinesRepository.insert(busStopLinesEntity);
    }


}
