import com.braders.persistence.config.IntegrationTestConfig;
import com.braders.persistence.dao.BusStopLinesRepository;
import com.braders.persistence.dao.InspectionNotificationRepository;
import com.braders.persistence.entity.BusStopLinesEntity;
import com.braders.persistence.entity.InspectionNotificationEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ActiveProfiles("IntegrationDatabaseTest")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {IntegrationTestConfig.class})
@Slf4j
public class InspectionNotificationRepositoryTest {

    private InspectionNotificationEntity inspectionNotificationEntity;
    private BusStopLinesEntity busStopLinesEntity;

    @Autowired
    private InspectionNotificationRepository inspectionNotificationRepository;

    @Autowired
    private BusStopLinesRepository busStopLinesRepository;

    @Before
    public void setup() {
        inspectionNotificationRepository = Mockito.mock(InspectionNotificationRepository.class, AdditionalAnswers.delegatesTo(inspectionNotificationRepository));
        busStopLinesRepository = Mockito.mock(BusStopLinesRepository.class, AdditionalAnswers.delegatesTo(busStopLinesRepository));

        busStopLinesEntity = busStopLinesRepository.get(533).orElse(null);

        inspectionNotificationEntity = new InspectionNotificationEntity();
        inspectionNotificationEntity.setBusStop(busStopLinesEntity);
        inspectionNotificationEntity.setDate(LocalDateTime.now());
    }

    @Test
    @Transactional
    @Rollback
    @Ignore
    public void testDatabaseOperation() {
        Mockito.verify(busStopLinesRepository, Mockito.times(1)).get(533);
        Assert.assertNotNull(busStopLinesEntity);

        Assert.assertTrue(inspectionNotificationRepository.insert(inspectionNotificationEntity));
        Mockito.verify(inspectionNotificationRepository, Mockito.times(1)).insert(inspectionNotificationEntity);

        List<InspectionNotificationEntity> ticketInspectionEntities = new ArrayList<>(inspectionNotificationRepository.getAll());
        Assert.assertFalse(ticketInspectionEntities.isEmpty());
        Mockito.verify(inspectionNotificationRepository, Mockito.times(1)).getAll();

        InspectionNotificationEntity lastAddedEntity = ticketInspectionEntities.get(ticketInspectionEntities.size() - 1);

        InspectionNotificationEntity recivedInspectionNotificationEntity = inspectionNotificationRepository.get(lastAddedEntity.getId()).get();
        Mockito.verify(inspectionNotificationRepository, Mockito.times(1)).get(recivedInspectionNotificationEntity.getId());

        Assert.assertEquals(lastAddedEntity, recivedInspectionNotificationEntity);

        Assert.assertTrue(inspectionNotificationRepository.delete(recivedInspectionNotificationEntity.getId()));
        Mockito.verify(inspectionNotificationRepository, Mockito.times(1)).delete(recivedInspectionNotificationEntity.getId());
    }
    @Ignore
    @Test(expected = UnsupportedOperationException.class)
    public void shouldBeThrownUnsupportedOperationException() {
        inspectionNotificationRepository.deleteAll();
    }
}
