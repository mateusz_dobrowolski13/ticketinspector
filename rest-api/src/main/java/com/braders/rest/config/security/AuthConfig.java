package com.braders.rest.config.security;

import com.braders.rest.controller.rest.properties.BusRestClientProperties;
import com.braders.rest.filter.CustomCorsFilter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.builders.ClientDetailsServiceBuilder;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;

import javax.sql.DataSource;
import java.util.Collections;

@EnableAuthorizationServer
@Configuration
public class AuthConfig extends AuthorizationServerConfigurerAdapter {

    private final DataSource dataSource;
    private final CustomCorsFilter customCorsFilter;
    private final BusRestClientProperties busRestClientProperties;
    @Qualifier("authenticationManagerBean") private final AuthenticationManager authenticationManager;
    @Qualifier("userDetailsService") private final UserDetailsService userDetailsService;

    public AuthConfig(DataSource dataSource,
                      CustomCorsFilter customCorsFilter,
                      AuthenticationManager authenticationManager,
                      UserDetailsService userDetailsService,
                      BusRestClientProperties busRestClientProperties) {
        this.dataSource = dataSource;
        this.customCorsFilter = customCorsFilter;
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.busRestClientProperties = busRestClientProperties;
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer authorizationServerSecurityConfigurer) {
        authorizationServerSecurityConfigurer
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("permitAll()")
                .addTokenEndpointAuthenticationFilter(customCorsFilter);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer authorizationServerEndpointsConfigurer) throws Exception {
        authorizationServerEndpointsConfigurer
                .tokenServices(tokenService())
                .authenticationManager(authenticationManager);
    }

    @Bean
    ClientDetailsService clientDetailsService() throws Exception {
        return new ClientDetailsServiceBuilder()
                .inMemory()
                .withClient(busRestClientProperties.client())
                .scopes(busRestClientProperties.scopes())
                .secret(busRestClientProperties.secret())
                .authorizedGrantTypes(busRestClientProperties.authorizedGrantTypes())
                .and()
                .build();
    }

    @Bean
    @Primary
    AuthorizationServerTokenServices tokenService() throws Exception {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        defaultTokenServices.setAccessTokenValiditySeconds(30);
        defaultTokenServices.setRefreshTokenValiditySeconds(180);
        defaultTokenServices.setClientDetailsService(clientDetailsService());
        defaultTokenServices.setAuthenticationManager(preAuthProvider());
        return defaultTokenServices;
    }

    private ProviderManager preAuthProvider() {
        PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
        provider.setPreAuthenticatedUserDetailsService(new UserDetailsByNameServiceWrapper<>(userDetailsService));
        return new ProviderManager(Collections.singletonList(provider));
    }

    @Bean
    TokenStore tokenStore() {
        return new JdbcTokenStore(dataSource);
    }
}
