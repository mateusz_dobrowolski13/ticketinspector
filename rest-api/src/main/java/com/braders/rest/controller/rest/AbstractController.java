package com.braders.rest.controller.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

public abstract class AbstractController {

    private Gson gson;

    public AbstractController() {
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    protected String convertToJson(Object object) {
        return gson.toJson(object);
    }

    protected <T>  T convertFromJson(String json, Type typeOfT) {
        return gson.fromJson(json, typeOfT);
    }
}
