package com.braders.rest.controller.rest;

import com.braders.rest.gson.GsonManager;
import com.braders.rest.test.Bot;
import com.braders.rest.test.route.RouteDemo;
import com.google.gson.stream.JsonReader;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/rest")
@Slf4j
public class BotController {

    private final GsonManager gsonManager;

    @RequestMapping(method = RequestMethod.GET, path = "simulate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String simulate(@Value("classpath:test/routes/*.json") Resource[] resources) {
        WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(simpleWebSocketClient));
        SockJsClient sockJsClient = new SockJsClient(transports);

        WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        String url = "https://ticket.inspector.com:8443/socket";
        StompSessionHandler sessionHandler = new BotController.MyStompSessionHandler();

        try {
            for (Resource resource : resources) {
                JsonReader jsonReader = new JsonReader(new FileReader(((FileSystemResource) resource).getPath()));
                RouteDemo[] routeDemos = gsonManager.convertFromFileJson(jsonReader, RouteDemo[].class);
                new Bot(routeDemos, stompClient.connect(url, sessionHandler).get()).start();
            }
        } catch (FileNotFoundException | InterruptedException | ExecutionException e) {
            log.error(e.getMessage(), e);
        }

        return "ok";
    }

    static public class MyStompSessionHandler
            extends StompSessionHandlerAdapter {

        private void subscribeTopic(StompSession session) {
            session.subscribe("/topic/messages", new StompFrameHandler() {

                @Override
                public Type getPayloadType(StompHeaders headers) {
                    return String.class;
                }

                @Override
                public void handleFrame(StompHeaders headers,
                                        Object payload) {
                    log.error(payload.toString());
                }
            });
        }

        @Override
        public void afterConnected(StompSession session,
                                   StompHeaders connectedHeaders) {
            subscribeTopic(session);
        }
    }
}
