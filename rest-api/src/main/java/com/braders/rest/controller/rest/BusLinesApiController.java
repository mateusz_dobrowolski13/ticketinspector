package com.braders.rest.controller.rest;


import com.braders.persistence.dao.BusLinesRepository;
import com.braders.persistence.entity.BusLineEntity;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Validated
@RequiredArgsConstructor
@ResponseStatus(value = HttpStatus.OK)
@RequestMapping(path = "/rest")
public class BusLinesApiController extends AbstractController {

    private final BusLinesRepository busStopRepository;

    @ApiOperation(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, value = "Get all bus lines", response = BusLinesRepository.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Bus stops")})

    @GetMapping(path = "/busLines", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getAllBusStops() {

        List<String> busLinesDTO = busStopRepository.getAll().stream()
                .map(BusLineEntity::getBusLine)
                .collect(Collectors.toCollection(LinkedList::new));

        return convertToJson(busLinesDTO);
    }
}