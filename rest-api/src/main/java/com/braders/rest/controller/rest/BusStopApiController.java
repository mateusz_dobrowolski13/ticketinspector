package com.braders.rest.controller.rest;


import com.braders.persistence.dao.BusStopRepository;
import com.braders.persistence.entity.BusStopEntity;
import com.braders.rest.dto.rest.BusStopDTO;
import com.braders.rest.gson.GsonManager;
import com.braders.rest.transofmators.impl.BusStopTransformer;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@Validated
@RequiredArgsConstructor
@ResponseStatus(value = HttpStatus.OK)
@RequestMapping(path = "/rest")
public class BusStopApiController extends AbstractController {
    private final GsonManager gsonManager;
    private final BusStopRepository busStopRepository;
    private final BusStopTransformer busStopTransformer;

    @ApiOperation(value = "Get all bus stops", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Bus stops")})

    @GetMapping(path = "/busStops", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getAllBusStops() {
        List<BusStopDTO> busStopDTOList = busStopRepository.getAll().stream()
                .map(busStopTransformer::convertToDTO)
                .collect(Collectors.toCollection(LinkedList::new));
        return convertToJson(busStopDTOList);
    }

    @ApiOperation(value = "Get bus stops", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Bus stop")})

    @GetMapping(path = "/busStops/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getBus(@Pattern(regexp = "^-?\\d+$", message = "ID should be a number") @PathVariable String id) {

        Optional<BusStopEntity> busStopEntityOptional = busStopRepository.get(Integer.parseInt(id));
        if (!busStopEntityOptional.isPresent()) {
            throw new IllegalArgumentException("Bus stop with specific id doesn't exist");
        }

        BusStopDTO busStopDTO = busStopTransformer.convertToDTO(busStopEntityOptional.get());
        return convertToJson(busStopDTO);
    }

}