package com.braders.rest.controller.rest;


import com.braders.persistence.dao.BusStopRepository;
import com.braders.rest.distributor.AbstractDataDistributor;
import com.braders.rest.distributor.CityEnum;
import com.braders.rest.distributor.RestHandler;
import com.braders.rest.dto.rest.BusStopLinesDTO;
import com.braders.rest.transofmators.impl.BusStopLinesTransformer;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;
import java.util.List;

@RestController
@Validated
@RequiredArgsConstructor
@ResponseStatus(value = HttpStatus.OK)
@RequestMapping(path = "/rest")
public class BusStopLinesApiController extends AbstractController {

    private final BusStopRepository busStopRepository;
    private final BusStopLinesTransformer busStopLinesTransformer;
    private final List<AbstractDataDistributor> restHandlers;

    @GetMapping(path = "/busStop/{busStopID}/lines", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getBusStopLines(@Pattern(regexp = "^?\\d+$", message = "Bus Stop ID should be a number") @PathVariable String busStopID)  {
        int id = Integer.parseInt(busStopID);

        try {
            String cityName = busStopRepository.getCity(id);
            RestHandler restHandler = restHandlers.stream()
                    .filter(rest -> rest.getCity() == CityEnum.valueOf(cityName))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Cannot find appropriate rest handler for city " + cityName) );
            List<BusStopLinesDTO> busStopLinesDTOS = restHandler.processResponse(busStopID);
            return convertToJson(busStopLinesTransformer.convertToListDTO(busStopLinesDTOS));
        } catch (Exception e) {
           throw new IllegalArgumentException(e.getMessage());
        }

    }

}