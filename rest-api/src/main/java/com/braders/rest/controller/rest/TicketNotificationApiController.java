package com.braders.rest.controller.rest;

import com.braders.persistence.dao.TicketInspectionRepository;
import com.braders.rest.dto.rest.TicketInspectionDTO;
import com.braders.rest.gson.GsonManager;
import com.braders.rest.transofmators.impl.TicketInspectionTransformer;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@Validated
@RequiredArgsConstructor
@ResponseStatus(value = HttpStatus.OK)
@RequestMapping(path = "/rest")
public class TicketNotificationApiController extends AbstractController {

    private final GsonManager gsonManager;
    private final TicketInspectionRepository ticketInspectionRepository;
    private final TicketInspectionTransformer ticketInspectionTransformer;

    @ApiOperation(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, value = "Get all ticket inspection notifications", notes = "", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ticket inspection notifications")
    }
    )

    @GetMapping(path = "/ticketNotification", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getAllInspectionNotification() {
        List<TicketInspectionDTO> ticketInspectionDTOS = ticketInspectionRepository.getAll()
                .stream()
                .map(ticketInspectionTransformer::convertToDTO)
                .collect(Collectors.toList());
        return gsonManager.convertToJson(ticketInspectionDTOS);
    }
}
