package com.braders.rest.controller.rest.properties;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@Getter
@Accessors(fluent = true)
@PropertySource(value = "classpath:clients/bus-rest-client.properties", ignoreResourceNotFound = true)
public class BusRestClientProperties {
    @Value("${client}")
    private String client;

    @Value("#{'${scopes}'.split(',')}")
    private String[] scopes;

    @Value("${secret}")
    private String secret;

    @Value("#{'${authorizedGrantTypes}'.split(',')}")
    private String[] authorizedGrantTypes;
}
