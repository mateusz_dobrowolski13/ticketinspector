package com.braders.rest.distributor;

import com.braders.rest.gson.GsonManager;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public abstract class AbstractDataDistributor implements RestHandler {
    @Autowired
    protected GsonManager gsonManager;
    @Autowired
    protected DozerBeanMapper dozerMapper;
    @Autowired
    protected RestTemplate restTemplate;

    private final CityEnum city;

    public AbstractDataDistributor(CityEnum city) {
        this.city = city;
    }

    public CityEnum getCity() {
        return city;
    }
}
