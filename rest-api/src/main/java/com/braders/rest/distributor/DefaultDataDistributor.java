package com.braders.rest.distributor;

import com.braders.persistence.dao.BusStopLinesRepository;
import com.braders.rest.dto.rest.BusStopLinesDTO;
import com.braders.rest.transofmators.impl.BusStopLinesTransformer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DefaultDataDistributor extends AbstractDataDistributor {

    private final BusStopLinesRepository busStopLinesRepository;
    private final BusStopLinesTransformer busStopLinesTransformer;

    public DefaultDataDistributor(BusStopLinesRepository busStopLinesRepository, BusStopLinesTransformer busStopLinesTransformer) {
        super(CityEnum.LUB);
        this.busStopLinesRepository = busStopLinesRepository;
        this.busStopLinesTransformer = busStopLinesTransformer;
    }

    @Override
    public List<BusStopLinesDTO> processResponse(String id) {
        return busStopLinesRepository.getAllBusStopLines(Integer.parseInt(id))
                .stream().map(busStopLinesTransformer::convertToDTO)
                .collect(Collectors.toList());
    }
}
