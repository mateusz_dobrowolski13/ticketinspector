package com.braders.rest.distributor;

import java.util.List;

public interface RestHandler<T> {
    List<T> processResponse(String id) throws Exception;
}
