package com.braders.rest.distributor.waw;

import com.braders.rest.distributor.AbstractDataDistributor;
import com.braders.rest.distributor.CityEnum;
import com.braders.rest.dozer.util.DozerUtil;
import com.braders.rest.dto.rest.BusStopLinesDTO;
import com.braders.rest.dto.waw.Result;
import com.braders.rest.dto.waw.Value;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WawRest extends AbstractDataDistributor {

    private final WawRestConfigProperties wawRestConfigProperties;

    public WawRest(WawRestConfigProperties wawRestConfigProperties) {
        super(CityEnum.WAW);
        this.wawRestConfigProperties = wawRestConfigProperties;
    }

    @Override
    public List<BusStopLinesDTO> processResponse(String id) throws Exception {
        String requestUrl = wawRestConfigProperties.url() + "/?id="
                + wawRestConfigProperties.idBusStopLines()
                + "&busstopId=" + id.substring(0, id.length() - 2)
                + "&busstopNr=" + id.substring(id.length() - 2)
                + "&apikey=" + wawRestConfigProperties.apiKey() + "";
        try {
            String responseBody = restTemplate.getForObject(requestUrl, String.class);

            JSONObject jsonObject = new JSONObject(responseBody);
            Optional<JSONArray> jsonArray = Optional.ofNullable(jsonObject.optJSONArray("result"));

            Optional<Result[]> optResults = jsonArray.<Optional<Result[]>>map(array -> Optional.of(gsonManager.convertFromJson(array.toString(), Result[].class)))
                    .orElseThrow(() -> new IllegalArgumentException(jsonObject.getString("result")));

            return DozerUtil.convertListToNewType(dozerMapper, getLinesArray(optResults), BusStopLinesDTO.class);
        } catch (RestClientException restClientException) {
            throw new Exception("Connection Failure with the WaW Rest");
        } catch (IllegalArgumentException ex) {
            throw new Exception(ex.getMessage());
        }
    }

    private List<Value> getLinesArray(Optional<Result[]> optResult) {
        if (optResult.isPresent()) {
            return Arrays.stream(optResult.get()).map(e -> e.getValues().get(0)).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }
}
