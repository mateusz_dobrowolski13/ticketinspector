package com.braders.rest.distributor.waw;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@Getter
@Accessors(fluent = true)
@PropertySource(value = "classpath:waw/wawRestConfig.properties", ignoreResourceNotFound = true)
public class WawRestConfigProperties {

    @Value("${api_key}")
    private String apiKey;

    @Value("${url}")
    private String url;

    @Value("${id.bus.stop.lines}")
    private String idBusStopLines;
}
