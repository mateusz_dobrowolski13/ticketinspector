package com.braders.rest.dozer.conventer;

import com.braders.rest.websocket.model.LocationMessage;
import org.dozer.CustomConverter;
import org.dozer.MappingException;

public class LocationStateConverter implements CustomConverter {
    @Override
    public Object convert(Object destination, Object source, Class<?> destinationClass, Class<?> sourceClass) {
        if (source == null) {
            return null;
        }

        if (source instanceof String) {
            switch (LocationMessage.LocationState.valueOf(source.toString())) {
                case UPDATE:
                    return true;
                case REMOVE:
                default:
                    return false;
            }
        } else {
            throw new MappingException("Converter LocationStateConverter "
                    + "used incorrectly. Arguments passed in were:"
                    + destination + " and " + source);
        }
    }
}
