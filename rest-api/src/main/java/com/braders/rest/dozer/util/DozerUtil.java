package com.braders.rest.dozer.util;

import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.List;

public class DozerUtil {

    private DozerUtil() {
    }

    public static <T, S> List<T> convertListToNewType(Mapper mapper, List<S> objects, Class<T> newObjectClass) {
        List<T> newObjectsList = new ArrayList<>();
        objects.stream().forEach(object -> newObjectsList.add(mapper.map(object, newObjectClass)));
        return newObjectsList;
    }
}
