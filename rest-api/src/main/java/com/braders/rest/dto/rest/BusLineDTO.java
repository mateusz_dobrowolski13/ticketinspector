package com.braders.rest.dto.rest;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class BusLineDTO {
    private String busLine;
}
