package com.braders.rest.dto.rest;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class BusStopDTO {
    private int id;
    private String name;
    private double latitude;
    private double longitude;
    private String city;
}
