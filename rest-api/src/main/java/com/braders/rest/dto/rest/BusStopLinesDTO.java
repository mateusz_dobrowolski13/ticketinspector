package com.braders.rest.dto.rest;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
public class BusStopLinesDTO {
    private int id;
    private int busStopID;
    private String busLine;
    private boolean inspectionTicket;
}
