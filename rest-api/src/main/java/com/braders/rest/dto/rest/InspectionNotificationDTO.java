package com.braders.rest.dto.rest;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Builder
@Setter
@Getter
public class InspectionNotificationDTO {
    private int id;
    private LocalDateTime date;
    private BusStopLinesDTO busStopLines;
}
