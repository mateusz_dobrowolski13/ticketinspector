package com.braders.rest.dto.rest;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Builder
@Setter
@Getter
public class ListBusStopLinesDTO {
    private boolean anyInspectionTicket = false;
    private Collection<BusStopLinesDTO> busStopLines;
}
