package com.braders.rest.dto.rest;

import lombok.Builder;

import java.util.Date;
import java.util.List;

@Builder
public class TicketInspectionDTO {
    private String uuid;
    private Date createDate;
    private Date updateDate;
    private String busStopName;
    private String busLine;
    private double longitude;
    private double latitude;
    private List<TrackDTO> route;
}
