package com.braders.rest.dto.rest;

import lombok.Builder;

@Builder
public class TrackDTO {
    private int id;
    private double longitude;
    private double latitude;
}
