package com.braders.rest.dto.waw;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Result {

    @SerializedName("values")
    @Expose
    private List<Value> values = null;

}


