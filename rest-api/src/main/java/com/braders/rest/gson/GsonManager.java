package com.braders.rest.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.lang.reflect.Type;

public class GsonManager {
    private Gson gson;

    public GsonManager() {
        this.gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .setPrettyPrinting()
                .create();
    }

    public <T> T convertFromFileJson(JsonReader reader, Type typeOfT) {
        return gson.fromJson(reader, typeOfT);
    }

    public String convertToJson(Object object) {
        return gson.toJson(object);
    }

    public <T> T convertFromJson(String json, Type typeOfT) {
        return gson.fromJson(json, typeOfT);
    }
}
