package com.braders.rest.test;

import com.braders.rest.test.route.RouteDemo;
import org.springframework.messaging.simp.stomp.StompSession;

public class Bot extends Thread {
    private RouteDemo[] routeDemos;
    private StompSession session;

    public Bot(RouteDemo[] routeDemos, StompSession session) {
        this.routeDemos = routeDemos;
        this.session = session;
    }

    @Override
    public void run() {
    }
}
