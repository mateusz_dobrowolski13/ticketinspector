package com.braders.rest.transofmators;

public interface GenericTransformator<D, E> {
    E convertToEntity(D dto);

    D convertToDTO(E entity);
}
