package com.braders.rest.transofmators.impl;

import com.braders.persistence.entity.BusLineEntity;
import com.braders.rest.dto.rest.BusLineDTO;
import com.braders.rest.transofmators.GenericTransformator;
import org.springframework.stereotype.Component;

@Component
public class BusLineTransformer implements GenericTransformator<BusLineDTO, BusLineEntity> {

    @Override
    public BusLineEntity convertToEntity(BusLineDTO dto) {
        return BusLineEntity.builder().busLine(dto.getBusLine())
                .build();
    }

    @Override
    public BusLineDTO convertToDTO(BusLineEntity entity) {
        return BusLineDTO.builder().busLine(entity.getBusLine())
                .build();
    }
}
