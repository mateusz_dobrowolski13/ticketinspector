package com.braders.rest.transofmators.impl;

import com.braders.persistence.entity.BusStopLinesEntity;
import com.braders.rest.dto.rest.BusStopLinesDTO;
import com.braders.rest.dto.rest.ListBusStopLinesDTO;
import com.braders.rest.transofmators.GenericTransformator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;

import static com.braders.persistence.util.Const.UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION;

@Component
@RequiredArgsConstructor
public class BusStopLinesTransformer implements GenericTransformator<BusStopLinesDTO, BusStopLinesEntity> {

    @Override
    public BusStopLinesEntity convertToEntity(BusStopLinesDTO dto) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_MESSAGE_EXCEPTION);
    }

    @Override
    public BusStopLinesDTO convertToDTO(BusStopLinesEntity entity) {
        return BusStopLinesDTO.builder().busStopID(entity.getBusStop().getId())
                .busLine(entity.getFkBusLine())
                .inspectionTicket(entity.getInspectionTicket())
                .id(entity.getId())
                .build();
    }

    public ListBusStopLinesDTO convertToListDTO(Collection<BusStopLinesDTO> busStopLinesDTOS) {
        return ListBusStopLinesDTO.builder()
                .anyInspectionTicket(busStopLinesDTOS.stream().anyMatch(BusStopLinesDTO::isInspectionTicket))
                .busStopLines(busStopLinesDTOS)
                .build();
    }
}
