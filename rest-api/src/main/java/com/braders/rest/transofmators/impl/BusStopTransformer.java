package com.braders.rest.transofmators.impl;

import com.braders.persistence.entity.BusStopEntity;
import com.braders.rest.dto.rest.BusStopDTO;
import com.braders.rest.transofmators.GenericTransformator;
import org.springframework.stereotype.Component;

@Component
public class BusStopTransformer implements GenericTransformator<BusStopDTO, BusStopEntity> {

    @Override
    public BusStopEntity convertToEntity(BusStopDTO busStopDTO) {
        return null;
    }

    @Override
    public BusStopDTO convertToDTO(BusStopEntity busStopEntity) {
        return BusStopDTO.builder().id(busStopEntity.getId())
                .name(busStopEntity.getName())
                .longitude(busStopEntity.getLongitude())
                .latitude(busStopEntity.getLatitude())
                .city(busStopEntity.getCity())
                .build();
    }
}
