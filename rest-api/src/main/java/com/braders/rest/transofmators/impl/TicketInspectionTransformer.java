package com.braders.rest.transofmators.impl;

import com.braders.persistence.entity.inspection.TicketInspectionEntity;
import com.braders.persistence.entity.inspection.TrackEntity;
import com.braders.rest.dto.rest.TicketInspectionDTO;
import com.braders.rest.dto.rest.TrackDTO;
import com.braders.rest.transofmators.GenericTransformator;
import org.springframework.stereotype.Controller;

import java.util.stream.Collectors;

@Controller
public class TicketInspectionTransformer implements GenericTransformator<TicketInspectionDTO, TicketInspectionEntity> {

    @Override
    public TicketInspectionEntity convertToEntity(TicketInspectionDTO dto) {
        return new TicketInspectionEntity();
    }

    @Override
    public TicketInspectionDTO convertToDTO(TicketInspectionEntity entity) {
        TrackEntity lastTrackEntity = entity.getRoute().get(entity.getRoute().size() - 1);
        return TicketInspectionDTO.builder().uuid(entity.getUuid())
                .createDate(entity.getCreateDate())
                .updateDate(entity.getUpdateDate())
                .busStopName(entity.getBusStopName())
                .busLine(entity.getBusLine())
                .longitude(lastTrackEntity.getLongitude())
                .latitude(lastTrackEntity.getLatitude())
                .route(entity.getRoute().stream()
                        .map(route -> TrackDTO.builder().id(route.getId()).longitude(route.getLongitude()).latitude(route.getLatitude()).build())
                        .collect(Collectors.toList()))
                .build();
    }
}
