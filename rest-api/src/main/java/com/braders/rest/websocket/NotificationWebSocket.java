package com.braders.rest.websocket;

import com.braders.persistence.dao.TicketInspectionRepository;
import com.braders.persistence.dao.TrackRepository;
import com.braders.persistence.entity.inspection.TicketInspectionEntity;
import com.braders.persistence.entity.inspection.TrackEntity;
import com.braders.rest.websocket.model.LocationMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dozer.DozerBeanMapper;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.Date;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
@Slf4j
public class NotificationWebSocket {

    private final TicketInspectionRepository ticketInspectionRepository;
    private final TrackRepository trackRepository;
    private final DozerBeanMapper dozerMapper;

    @MessageMapping("/notification")
    @SendTo("/topic/notification")
    public LocationMessage notification(LocationMessage message) {
        Optional<TicketInspectionEntity> ticketInspectionEntityOptional = ticketInspectionRepository.get(message.getUuid());
        ticketInspectionEntityOptional.ifPresentOrElse(ticketInspectionEntity -> updateRecords(message, ticketInspectionEntity), () -> insertRecords(message));
        log.info("Sent message body: " + message.toString());
        return message;
    }

    private void updateRecords(LocationMessage message, TicketInspectionEntity ticketInspectionEntity) {
        trackRepository.insert(dozerMapper.map(message, TrackEntity.class));
        dozerMapper.map(message, ticketInspectionEntity);
        ticketInspectionEntity.setUpdateDate(new Date());
        ticketInspectionRepository.update(ticketInspectionEntity);
    }

    private void insertRecords(LocationMessage message) {
        ticketInspectionRepository.insert(dozerMapper.map(message, TicketInspectionEntity.class));
        trackRepository.insert(dozerMapper.map(message, TrackEntity.class));
    }
}
