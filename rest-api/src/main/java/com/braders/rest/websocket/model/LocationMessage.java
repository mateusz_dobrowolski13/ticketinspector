package com.braders.rest.websocket.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@Setter
@Getter
@ToString
public class LocationMessage {
    private String uuid;
    private double latitude;
    private double longitude;
    private String busLine;
    private String busStopName;
    private String state;
    private Date updateDate;
    private Date createDate;

    public enum LocationState {
        UPDATE, REMOVE
    }
}
