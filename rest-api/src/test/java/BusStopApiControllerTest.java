import com.braders.persistence.dao.BusStopRepository;
import com.braders.persistence.entity.BusStopEntity;
import com.braders.rest.config.RestConfig;
import com.braders.rest.controller.rest.BusStopApiController;
import com.braders.rest.gson.GsonManager;
import com.braders.rest.transofmators.impl.BusStopTransformer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles({"test", "jpa"})
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RestConfig.class})
@WebAppConfiguration

public class BusStopApiControllerTest {
    private final String REST_SUFFIX = "/rest";
    private MockMvc mockMvc;

    @Spy
    @Autowired
    private BusStopRepository busStopRepository;

    @Spy
    @Autowired
    private BusStopTransformer busStopTransformer;

    @Spy
    @Autowired
    private GsonManager gsonManager;

    @InjectMocks
    private BusStopApiController busStopApiController = new BusStopApiController(gsonManager, busStopRepository, busStopTransformer);


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(busStopApiController)
                .build();
    }

    @Test
    public void shouldReturnSpecificBusStop() throws Exception {
        BusStopEntity found = BusStopEntity.builder()
                .id(1023)
                .name("Plac Litewski 03")
                .latitude(51.2479)
                .longitude(22.5593)
                .build();

        Optional<BusStopEntity> busStopEntityOptional = Optional.of(found);


        when(busStopRepository.get(1023)).thenReturn(busStopEntityOptional);


        mockMvc.perform(get(REST_SUFFIX+"/busStops/{id}", "1023"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(1023)))
                .andExpect(jsonPath("$.name", is("Plac Litewski 03")))
                .andExpect(jsonPath("$.latitude", is(51.2479)))
                .andExpect(jsonPath("$.longitude", is(22.5593)));

        verify(busStopRepository, times(1)).get(1023);
        verifyNoMoreInteractions(busStopRepository);
    }

    @Test
    public void shouldReturnAllBusStop() throws Exception {
        Collection<BusStopEntity> busStops = new ArrayList<BusStopEntity>() {{
            add(BusStopEntity.builder()
                    .id(1023)
                    .name("Plac Litewski 03")
                    .latitude(51.2479)
                    .longitude(22.5593)
                    .build());
            add(BusStopEntity.builder()
                    .id(1024)
                    .name("Plac Litewski 02")
                    .latitude(51.2478)
                    .longitude(22.5592)
                    .build());
            add(BusStopEntity.builder()
                    .id(1025)
                    .name("Plac Litewski 01")
                    .latitude(51.2477)
                    .longitude(22.5591)
                    .build());
        }};

        when(busStopRepository.getAll()).thenReturn(busStops);

        mockMvc.perform(get(REST_SUFFIX+"/busStops"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].id", is(1023)))
                .andExpect(jsonPath("$[0].name", is("Plac Litewski 03")))
                .andExpect(jsonPath("$[0].latitude", is(51.2479)))
                .andExpect(jsonPath("$[0].longitude", is(22.5593)))

                .andExpect(jsonPath("$[1].id", is(1024)))
                .andExpect(jsonPath("$[1].name", is("Plac Litewski 02")))
                .andExpect(jsonPath("$[1].latitude", is(51.2478)))
                .andExpect(jsonPath("$[1].longitude", is(22.5592)))

                .andExpect(jsonPath("$[2].id", is(1025)))
                .andExpect(jsonPath("$[2].name", is("Plac Litewski 01")))
                .andExpect(jsonPath("$[2].latitude", is(51.2477)))
                .andExpect(jsonPath("$[2].longitude", is(22.5591)));

        verify(busStopRepository, times(1)).getAll();
        verifyNoMoreInteractions(busStopRepository);

        Assert.assertEquals(false, busStopRepository.getAll().isEmpty());
    }

    @Test(expected = NestedServletException.class)
    public void shouldBeException() throws Exception {

        mockMvc.perform(get(REST_SUFFIX+"/busStops/a", "a"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string("ID should be a number"));

        verifyZeroInteractions(busStopRepository);
    }

    @Test
    public void shouldReturnNoBusStops() throws Exception {

        when(busStopRepository.getAll()).thenReturn(Collections.EMPTY_LIST);

        mockMvc.perform(get(REST_SUFFIX+"/busStops"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string("[]"));

        verify(busStopRepository, times(1)).getAll();
        verifyNoMoreInteractions(busStopRepository);

        Assert.assertEquals(true, busStopRepository.getAll().isEmpty());
    }

    @Ignore("Database without a procedure")
    @Test
    public void shouldReturn10NearestBusStops() throws Exception {

        mockMvc.perform(get(REST_SUFFIX+"/nearestBusStops")
                .param("longitude", "22.5374")
                .param("latitude", "51.2325"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].id", is(5121)))
                .andExpect(jsonPath("$[0].name", is("Pozytywistów 01")))

                .andExpect(jsonPath("$[1].id", is(5122)))
                .andExpect(jsonPath("$[1].name", is("Pozytywistów 02")))

                .andExpect(jsonPath("$[2].id", is(5402)))
                .andExpect(jsonPath("$[2].name", is("Brzeskiej 02")))

                .andExpect(jsonPath("$[3].id", is(5472)))
                .andExpect(jsonPath("$[3].name", is("Brzeskiej II 02")))

                .andExpect(jsonPath("$[4].id", is(5401)))
                .andExpect(jsonPath("$[4].name", is("Brzeskiej 01")))

                .andExpect(jsonPath("$[5].id", is(5471)))
                .andExpect(jsonPath("$[5].name", is("Brzeskiej II 01")))

                .andExpect(jsonPath("$[6].id", is(5321)))
                .andExpect(jsonPath("$[6].name", is("Rzeckiego 01")))

                .andExpect(jsonPath("$[7].id", is(5322)))
                .andExpect(jsonPath("$[7].name", is("Rzeckiego 02")))

                .andExpect(jsonPath("$[8].id", is(5414)))
                .andExpect(jsonPath("$[8].name", is("ZUS 04")))

                .andExpect(jsonPath("$[9].id", is(5332)))
                .andExpect(jsonPath("$[9].name", is("Sympatyczna 02")));

        verify(busStopRepository, times(1)).getNearestBusStops(22.5374, 51.2325);
        verifyNoMoreInteractions(busStopRepository);
    }

    @Ignore("Database without a procedure")
    @Test
    public void shouldReturn4NearestBusStops() throws Exception {

        mockMvc.perform(get(REST_SUFFIX+"/nearestBusStops")
                .param("longitude", "22.5153")
                .param("latitude", "51.1507"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].id", is(4881)))
                .andExpect(jsonPath("$[0].name", is("Poranna 01")))

                .andExpect(jsonPath("$[1].id", is(4882)))
                .andExpect(jsonPath("$[1].name", is("Poranna 02")))

                .andExpect(jsonPath("$[2].id", is(4892)))
                .andExpect(jsonPath("$[2].name", is("Wykietówka 02")))

                .andExpect(jsonPath("$[3].id", is(4891)))
                .andExpect(jsonPath("$[3].name", is("Wykietówka 01")));

        verify(busStopRepository, times(1)).getNearestBusStops(22.5153, 51.1507);
        verifyNoMoreInteractions(busStopRepository);
    }

    @Ignore("Database without a procedure")
    @Test
    public void shouldReturn0NearestBusStops() throws Exception {

        mockMvc.perform(get(REST_SUFFIX+"/nearestBusStops")
                .param("longitude", "22.5129")
                .param("latitude", "51.1176"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(content().string("[]"));

        verify(busStopRepository, times(1)).getNearestBusStops(22.5129, 51.1176);
        verifyNoMoreInteractions(busStopRepository);
    }
}

