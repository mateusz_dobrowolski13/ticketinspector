import com.braders.persistence.entity.BusStopEntity;
import com.braders.rest.dto.rest.BusStopDTO;
import com.braders.rest.transofmators.impl.BusStopTransformer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

@RunWith(MockitoJUnitRunner.class)
public class BusTransformatorTest {

    @Spy
    @Autowired
    private BusStopTransformer busStopTransformer = new BusStopTransformer();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testShouldReturnCorrectBus() {
        BusStopEntity busStopEntity = new BusStopEntity();
        busStopEntity.setId(5);
        busStopEntity.setName("Plac Unii Lubelskiej 01");
        busStopEntity.setLatitude(50.20);
        busStopEntity.setLongitude(50.20);

        BusStopDTO busStopDTO = BusStopDTO.builder().id(5)
                .name("Plac Unii Lubelskiej 01")
                .longitude(50.20)
                .latitude(50.20)
                .build();

        BusStopDTO recivedBusStopDTO = busStopTransformer.convertToDTO(busStopEntity);

        Mockito.verify(busStopTransformer, Mockito.times(1)).convertToDTO(busStopEntity);
        Mockito.verifyNoMoreInteractions(busStopTransformer);

        Assert.assertEquals(busStopDTO.getName(), recivedBusStopDTO.getName());
        Assert.assertEquals(busStopDTO.getId(), recivedBusStopDTO.getId());
        Assert.assertEquals(0, Double.compare(busStopDTO.getLatitude(), recivedBusStopDTO.getLatitude()));
        Assert.assertEquals(0, Double.compare(busStopDTO.getLongitude(), recivedBusStopDTO.getLongitude()));

    }
}
